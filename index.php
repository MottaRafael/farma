<?php
session_start();

require ('classes/conexao.php');

$pdo = conecta();
$resultado = $pdo->prepare("SELECT max(ID_LISTA)AS ID FROM listas_preco where cod_cliente = 1");
$resultado->execute();
$id = $resultado->fetch(PDO::FETCH_ASSOC);
//var_dump($id);
$teste = $_GET['email'];
$tes = $pdo->prepare("select CODIGO, NRO_LOJAS from CLIENTES where EMAIL_APP='" . $teste . "'");
$tes->execute();
$testando = $tes->fetch(PDO::FETCH_ASSOC);

$result = $pdo->prepare("select produtos.codigo, produtos.descricao as DESC,produtos.DESCRICAO_COMPLETA, PRODUTOS.PCT_IPI, 
                        produtos.cod_grupo, grupos_produto.descricao, listas_itens.preco
                        from listas_itens
                        inner join produtos on (listas_itens.cod_produto = produtos.codigo)
                        inner join grupos_produto on (produtos.cod_grupo = grupos_produto.codigo)
                        where 
                        produtos.ativo = 'S' and
                        (listas_itens.id_lista = '". $id['ID']."') and
                        ((produtos.cod_grupo = 30 or produtos.cod_grupo = 1 or produtos.cod_grupo = 7)
                        or
                        (produtos.cod_grupo = 10 and produtos.codigo between 9000 and 9999)) and produtos.codigo != 8025
                        and CHAR_LENGTH(produtos.codigo) <= 4 and produtos.codigo != 787 and produtos.codigo != 786 and 
                        produtos.codigo != 6077 and produtos.codigo != 8003 and produtos.codigo != 3548 and
                        produtos.codigo != 6081 and produtos.codigo != 6082 and produtos.codigo != 634 and
                        produtos.codigo != 6083 and listas_itens.preco > 1 order by grupos_produto.descricao desc");
$result->execute();

$idCustomer = $testando['CODIGO'];

?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0" user-scalable=0">
        <title>Pedidos</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <style>
            #interna{
                font-family: Avant Garde,Avantgarde,Century Gothic,CenturyGothic,AppleGothic,sans-serif; 
                font-size: 80%;
                color: #808080;
            }
            
            #interna2{
                text-align: right;
                font-family: Avant Garde,Avantgarde,Century Gothic,CenturyGothic,AppleGothic,sans-serif; 
                font-size: 65%;
                
            }
            
            #tabela{
                font-size: 18px;
                vertical-align: top;
            }
            
            #result{
                font-size: 18x;
                vertical-align: top;
            }
            
            #img_modal{
                width: 100%;
                height: 100%;
            }
            
            .thumbnail{
                width: 95%;
                height: 100%;
                float: left;
                display: block;
            }
            
            .wrapper {
                height: 10%;
                min-height: 10%;
                display: -webkit-flex;
                -webkit-align-items: center;
                align-items: center;
                -webkit-justify-content: center;
                justify-content: center;
            }
            
            .wrapper div {
                padding: 0%;
            }
            
            input[type='number'] {
                -moz-appearance: textfield;
            }
            
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {
                -webkit-appearance: none;
            }
            
            #principal{
                display: none;
            }
            
            .modal {
                text-align: center;
                position: fixed;
            }
            
            .modal:before {
                content: '';
                display: inline-block;
                height: 100%;
                vertical-align: middle;
                margin-right: -4px;
            }
            
            .modal-dialog {
                display: inline-block;
                text-align: left;
            }
            
            #img_det {
                justify-content: center;
            }
            
            .fixo {
                z-index: 199;
                position: fixed;
                top: 30px;
                right: 15px;
            }
            
           .btn-prima {
            color: #fff;
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          .btn-prima:focus,
          .btn-prima.focus {
            color: #fff;
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          .btn-prima:hover {
            color: #fff;
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          .btn-prima:active,
          .btn-prima.active,
          .open > .dropdown-toggle.btn-prima {
            color: #fff;
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          .btn-prima:active:hover,
          .btn-prima.active:hover,
          .open > .dropdown-toggle.btn-prima:hover,
          .btn-prima:active:focus,
          .btn-prima.active:focus,
          .open > .dropdown-toggle.btn-prima:focus,
          .btn-prima:active.focus,
          .btn-prima.active.focus,
          .open > .dropdown-toggle.btn-prima.focus {
            color: #fff;
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          .btn-prima:active,
          .btn-prima.active,
          .open > .dropdown-toggle.btn-prima {
            background-image: none;
          }
          .btn-prima.disabled:hover,
          .btn-prima[disabled]:hover,
          fieldset[disabled] .btn-prima:hover,
          .btn-prima.disabled:focus,
          .btn-prima[disabled]:focus,
          fieldset[disabled] .btn-prima:focus,
          .btn-prima.disabled.focus,
          .btn-prima[disabled].focus,
          fieldset[disabled] .btn-prima.focus {
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          .btn-prima .badge {
            color: #00B4BD;
            background-color: #fff;
          }
          
          .btn-primar {
            color: #fff;
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar:focus,
          .btn-primar.focus {
            color: #fff;
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar:hover {
            color: #fff;
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar:active,
          .btn-primar.active,
          .open > .dropdown-toggle.btn-primar {
            color: #fff;
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar:active:hover,
          .btn-primar.active:hover,
          .open > .dropdown-toggle.btn-primar:hover,
          .btn-primar:active:focus,
          .btn-primar.active:focus,
          .open > .dropdown-toggle.btn-primar:focus,
          .btn-primar:active.focus,
          .btn-primar.active.focus,
          .open > .dropdown-toggle.btn-primar.focus {
            color: #fff;
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar:active,
          .btn-primar.active,
          .open > .dropdown-toggle.btn-primar {
            background-image: none;
          }
          .btn-primar.disabled:hover,
          .btn-primar[disabled]:hover,
          fieldset[disabled] .btn-primar:hover,
          .btn-primar.disabled:focus,
          .btn-primar[disabled]:focus,
          fieldset[disabled] .btn-primar:focus,
          .btn-primar.disabled.focus,
          .btn-primar[disabled].focus,
          fieldset[disabled] .btn-primar.focus {
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar .badge {
            color: #008B8B;
            background-color: #fff;
          }
                    
        </style>
    </head>
    <script>
        if (document.readyState) {
            document.onreadystatechange = checkstate;
        } else if (document.addEventListener) {
            document.addEventListener("DOMContentLoaded", saydone, false);
        }
        
        function checkstate() {
            if (document.readyState == "complete" || document.readyState == "complete") {
                document.getElementById("principal").style.display = "block";
            }
        }
        
        function saydone() {
            document.getElementById("principal").style.display = "block";
        }
        
        $(document).ready(function () {
            $("form[ajax=true]").submit(function (e) {
                e.preventDefault();
                var dados = $(this).serialize();
                var form_url = 'addCarrinho.php';
                var form_method = 'POST';
                $.ajax({
                    url: form_url,
                    type: form_method,
                    cache: false,
                    data: dados,
                    success: function (dados) {
                        var obj = JSON.parse(dados);
                        $("#result").html("<a href='carrinho.php?codigo=<?php echo $testando['CODIGO']; ?>&desconto=S' style='color: #ffffff'><span id='tabela' class='glyphicon glyphicon-shopping-cart'>" + obj.contador + "</span></a>");
                        $("#result2").html(""+ obj.info + "<a href='carrinho.php?codigo=<?php  echo $testando['CODIGO']; ?>&desconto=S' style='color: #ffffff'><span id='tabela' class='glyphicon glyphicon-shopping-cart'>" + obj.contador + "</span></a>"+ obj.info2 + "");
                        $("#myModal");
                        setTimeout(function () {
                            $('#myModal').modal('hide');
                        }, 500);
                    }
                });
                return false;
            });
            $("#exemplo").submit(function (e) {
                e.preventDefault();
                var dados = $(this).serialize();
                var form_url = 'carrinho.php';
                var form_method = 'POST';
                $.ajax({
                    url: form_url,
                    type: form_method,
                    cache: false,
                    data: dados,
                    success: function () {
                        $('#exemplomodal').modal('hide');
                    }
                });
                return false;
            });
             $("#add").click(function (e) {
                 $('#exampleModalLong').modal('hide');
                             e.preventDefault();
//                           var dados = $(this).serialize();
                             var form_url = "session_destroy.php";
                             var form_method = "POST";
                             $.ajax({
                                 url: form_url,
                                 type: form_method,
                                 cache: false,
//                               data: dados,
                              success: function(){
                                 $("#myModal2").modal();
                                setTimeout(function () {
                                 location.reload();
                                 $('#myModal2').modal('hide');
                               }, 1300);
                             }
                       });
                  return false;
            }); 
        });

    </script>
    
    <body style="background-color: #00B4BD; margin-left: 0px;" id="principal">
            <div class="container theme-showcase" role="main">
                <div class="page-header" style="width: 81%; margin-left: 8.3%">
                    <div id="interna2" style="margin-bottom:-45px">
                        <h4>
                            <?php
                           
                            if (!empty($_SESSION['contador'])) {
                                echo "<span id='result'><a href='carrinho.php?codigo=" . $testando['CODIGO'] . "&desconto=S' style='color: #ffffff '><span id='tabela' class='glyphicon glyphicon-shopping-cart'>" . $_SESSION['contador'] . "</span></a></span>";
                            } else {
                                echo '<span id="result"><a href="carrinho.php?codigo=' . $testando['CODIGO'] . '&desconto=S" style="color: #ffffff"><span id="tabela" class="glyphicon glyphicon-shopping-cart">0</span></a></span>';
                            }
                            
                            ?>
                        </h4>
                    </div>
                    
                    <div id="interna">
                        <h3 style="color: #ffffff;text-align: left;font-size: 18px;">PRODUTOS</h3>
                    </div>
                </div>
                <?php 
                    
                    $sql_apaga = $pdo->prepare("select OUTRAS_RAZOES.cod_razao,CLIENTES.COD_GRUPO, clientes.matriz_filial as MATRIZ from clientes "
                    . "inner join outras_razoes on (clientes.codigo = outras_razoes.cod_razao) "
                    . "where OUTRAS_RAZOES.cod_cliente=" . $idCustomer);
                    $sql_apaga->execute();
                    $apagando = $sql_apaga->fetch(PDO::FETCH_ASSOC);
                    
                    if(!empty($apagando['COD_RAZAO'])){
                        echo '<form id="teste">'
                            . '<button type="button" class="btn btn-primar" style="margin: 5%; margin-left: 35%" data-toggle="modal" data-target="#exampleModalLong">TROCAR LOJA</button>'
                            . '</form>';
                    }
                    
                    ?>
          <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                  <h5>Deseja trocar de loja?</h5>
                
                      <?php 
                  
                  if ((!empty($_SESSION['contador']))&&($_SESSION['contador'] == 1)){
                      echo "<span id='result2'>Você tem <a href='carrinho.php?codigo=" . $testando['CODIGO'] . "&desconto=S' style='color: #ffffff '><span id='tabela' class='glyphicon glyphicon-shopping-cart'>" . $_SESSION['contador'] . "</span></a> item no carrinho</span>";
                     }elseif((!empty($_SESSION['contador']))&&($_SESSION['contador'] > 1 )){
                         echo "<span id='result2'>Você tem <a href='carrinho.php?codigo=" . $testando['CODIGO'] . "&desconto=S' style='color: #ffffff '><span id='tabela' class='glyphicon glyphicon-shopping-cart'>" . $_SESSION['contador'] . "</span></a> itens no carrinho</span>";
                     } else{ 
                      echo "<span id='result2'>Você não tem itens no carrinho</span>";
                     }
                          
                  ?>
                     
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-prima" id="add" data-toggle="modal" data-target="#myModal2">Trocar</button>
              </div>
            </div>
          </div>
        </div>
                
<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
            <br/>
         <div class="alert alert-success" role="alert">
             <strong>
                 <p> Finalizando Sessão...</p>
             </strong>
         </div>
        </div>
      </div>
    </div>
  </div>
                
                
            <form id="form" ajax="true" enctype="multipart/form-data">
            <input type="hidden" name="desconto" value="S" />
                <div class="wrapper">
                    <div class="col-xs-10">
                        <?php
                        $i = 0;
                        $x = 0;
                        while ($linha = $result->fetch(PDO::FETCH_ASSOC)) {
                            
                            $i++;
                            $x++;
                            $string = $linha['DESC'];
                            $string2 = strlen($string);
                            
                            ?>
                            <div class="col-xs-6">
                                <div class="thumbnail">
                                    <p></p>
                                    <?php echo '<font size="-17"><center>Cód. ' . $linha['CODIGO']. '</center></font>' ?> 
                                    <img id="img" src="imagens/<?php echo $linha['CODIGO']; ?>.png">
                                    <div class="caption text-center" style="height: 75px">
                                        <h6 id="interna"><?php echo $string; ?></h6>
                                        <h6 id="interna" style="color: #00b4bd;">R$ <?php echo number_format($linha['PRECO'], 2, ',', '.'); ?> Uni.</h6> 
                                    </div>

                    <?php
                    $select = $pdo->prepare("select produtos.codigo, grades_nros.numero, grades_nros.id_nrograde "
                            . "from produtos "
                            . "inner join grades_nros on (produtos.cod_grade = grades_nros.cod_grad) "
                            . "where ((produtos.codigo = " . $linha['CODIGO'] . "))");
                    $select->execute();
                    echo '<div class="modal fade" id="Modal_qtd' . $x . '"  role="dialog" data-backdrop="static">
                                            <div class="modal-dialog">
                                              <div class="modal-content">
                                                <div class="modal-header" style="text-align: center;">
                                                  <button type="button" class="close" data-dismiss="modal" style="margin:5%">&times;</button>
                                                  <h4 class="modal-title"><label style="margin:3%">Carrinho</label></h4>
                                                </div>
                                                <div class="modal-body"><br/>';
                    while ($consulta = $select->fetch(PDO::FETCH_ASSOC)) {
                        echo '<div style="margin-left: 15%">'
                        . '<input type="hidden" name="tamanho[]" id="tamanho" value="' . $consulta['ID_NROGRADE'] . '"/><label>' . $consulta['NUMERO'] . '</label>'
                        . '<input type="number" style="margin-left: 25%;width: 35%" size="10" id="qtd" name="qtd[]" value="" placeholder="Qtd" size="4"/></div>'
                        . '<input type="hidden" id="acao" name="acao[]" value="add"/>'
                        . '<input type="hidden" id="id" name="id[]" value="' . $consulta['CODIGO'] . '"/>';
                    }
                        echo '<br/></div>
                                        <div class="modal-footer">
                                        <button type="submit" id="add' . $x . '" style="width:30%;  margin-top: 5%;margin-right: 32%;margin-bottom: 5%" class="btn btn-prima" 
                                            data-toggle="modal" data-target="#myModal" ><span class="glyphicon glyphicon-shopping-cart"></span></button>
                                        <div class="col-xs-1" style="margin: 10%" ></div>
                                    </div>
                                  </div>
                                </div>
                              </div>';
                            ?>
                                    <div class="col-xs-12" style="align-content: center">
                                        <div class="col-xs-2"></div>
                                        <br/>
                                        <div class="col-xs-6">
                                            <button type="button" data-toggle="modal" data-target="#Modal_qtd<?php echo $x; ?>" style="margin-left: 25%;margin-bottom: 10%;" class="btn btn-prima"><span class="glyphicon glyphicon-shopping-cart"></span></button>
                                        </div>
                                        <div class="col-xs-6">
                                            <button type="button" name="det" id="det" data-toggle="modal" data-target="#Modal<?php echo $i; ?>" style="margin-left: 15%;margin-bottom: 10%;" class="btn btn-prima"><span class="glyphicon glyphicon glyphicon-zoom-in"></span></button>
                                        </div>
                                    </div>
                                    <script>
                                        $('#add<?php echo $x; ?>').on('click', function () {
                                            $('.modal').modal('hide');
                                        });
                                        $(window).scroll(function () {
                                            if ($(this).scrollTop() > 50) {
                                                $("#result").addClass("fixo");
                                            } else {
                                                $("#result").removeClass("fixo");
                                            }
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="modal fade" id="Modal<?php echo $i; ?>" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" style="margin:3%">&times;</button>
                                            <h4 class="modal-title"><label style="margin:3%; color: #808080">Detalhes do Produto</label></h4>
                                        </div>
                                        <div class="modal-body2">
                                            <div class="col-xs-12">
                                                <br/>
                                                <div class="col-xs-1"></div>
                                                <div class="col-xs-10">
                                                    <?php if((($linha['CODIGO']) == 3388) || (($linha['CODIGO']) == 3387)){ ?>
                                                    <img id="img_modal" src="detalhes/<?php echo $linha['CODIGO']; ?>.png">
                                                    <?php }else{ ?>
                                                    <label align="left" style="color: #00B4BD"><?php echo $linha['DESC'] ?></label>
                                                    <p align="justify" style="color: #808080"><?php echo $linha['DESCRICAO_COMPLETA'] ?></p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" style="margin: 10%" data-dismiss="modal">Fechar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                    <?php 
                        }
                    ?>
                    </div>
                </div>
                </form>
            </div>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <br/>
                            <div class="alert alert-success" role="alert">
                                <strong><p style="padding-left: 10"> Adicionado!</p></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        <form id="exemplo">
<?php
$sql_novo = $pdo->prepare("select OUTRAS_RAZOES.cod_razao,CLIENTES.COD_GRUPO, clientes.matriz_filial as MATRIZ from clientes "
        . "inner join outras_razoes on (clientes.codigo = outras_razoes.cod_razao) "
        . "where OUTRAS_RAZOES.cod_cliente=" . $idCustomer);
$sql_novo->execute();
$testando2 = $sql_novo->fetch(PDO::FETCH_ASSOC);
$matriz_filial = $testando2['MATRIZ'];
$matriz = trim($matriz_filial);
$et[] = $testando2['COD_RAZAO'];
$et2 = count($et);
if (($et2 >= 1) && (empty($_SESSION['razao_p'])) && (($matriz == "M") || ($matriz == "F")) && (($testando2['COD_GRUPO'] == 2) || ($testando2['COD_GRUPO'] == 12) || ($testando2['COD_GRUPO'] == 62))) {
    ?>
    <script>
        $(window).load(function () {
            $('#exemplomodal').modal('show');
        });
    </script>
    <?php
    $novo = $pdo->prepare("select OUTRAS_RAZOES.cod_razao,CLIENTES.cgc,CLIENTES.ENDERECOFATURAMENTO, CLIENTES.NOME "
            . "from clientes "
            . "inner join outras_razoes on (clientes.codigo = outras_razoes.cod_razao) "
            . "where OUTRAS_RAZOES.cod_cliente=" . $idCustomer);
    $novo->execute();
    $novo2 = $pdo->prepare("select NOME, CODIGO, CGC, ENDERECOFATURAMENTO FROM CLIENTES WHERE CODIGO=" . $idCustomer);
    $novo2->execute();
    $novo_sql2 = $novo2->fetch(PDO::FETCH_ASSOC);
    echo '<div class="modal fade" id="exemplomodal"  role="dialog" data-backdrop="static">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header" style="text-align: center;">
                                  <h4 class="modal-title"><label style="margin:3%">Escolha para qual loja deseja comprar</label></h4>
                                </div>
                                <div class="modal-body"><br/>
                                <label for="nome"><font size="1">RAZÃO: ' . $novo_sql2['NOME'] . '</font></label><br/>'
    . '<input type="radio" style="float: right;" value="' . $idCustomer . '" id="nome" name="cod_razao" checked/>'
    . '<label for="nome"><font size="1"> CNPJ: ' . $novo_sql2['CGC'] . '</font></label><br/>'
    . '<label for="nome"><font size="1">' . $novo_sql2['ENDERECOFATURAMENTO'] . '</font></label><br/>'
    . '<hr/>';
    $y = 0;
    while ($novo_sql = $novo->fetch(PDO::FETCH_ASSOC)) {
        $y++;
        echo
        '<label for="nome' . $y . '"><font size="1">RAZÃO: ' . $novo_sql['NOME'] . '</font></label><br/>'
        . '<input type="radio"  style="float: right;" value="' . $novo_sql['COD_RAZAO'] . '" id="nome' . $y . '" name="cod_razao"/>'
        . '<label for="nome' . $y . '"><font size="1"> CNPJ: ' . $novo_sql['CGC'] . '</font></label><br/>'
        . '<label for="nome' . $y . '"><font size="1">' . $novo_sql['ENDERECOFATURAMENTO'] . '</font></label>'
                . '<hr/>';
    }
        echo '<br/>
                        <div class="modal-footer">
                        <input type="submit" id="addexemplo" style="width:30%" class="btn btn-prima" value="OK">
                    </div>
                  </div>
                </div>
              </div>';
}

?>
        </form>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>