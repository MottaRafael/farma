<?php
require 'conexao.php';

/**
 * Description of Carrinho
 *
 * @author Rafael
 */
final class Carrinho {
    //put your code here
    public function __construct() {
        if(!isset($_SESSION['carrinho'])){
            $_SESSION['carrinho'] = array();
        }
    }
    
    public function adicionarProdutos($id, $qtd = 1, $tam = null){
        if(is_null($tam)){
            $indice = sprintf("%s:%s", (int)$id, 0);
        }else{
            $indice = sprintf("%s:%s", (int)$id,(int)$tam);
        }
        if(!isset($_SESSION['carrinho'][$indice])){
            $_SESSION['carrinho'][$indice] = (int)$qtd;
        }
    }
    
    
    public function alterarQtd($indice, $qtd){
        if(isset($_SESSION['carrinho'][$indice])){
            if($qtd>0){
                $_SESSION['carrinho'][$indice] = (int)$qtd;
            }
        }
    }
    
    public function excluirProduto($indice){
        unset($_SESSION['carrinho'][$indice]);
    }
    
    public function listarProdutos(){
        $retorno = array();
        foreach ($_SESSION['carrinho'] as $indice => $qtd){
            //separar o id do produto do id do tamanho
            list($id, $tam) = explode(":", $indice);
            $pdo = conecta();
            $resultado1 = $pdo ->prepare("SELECT max(ID_LISTA)AS ID FROM listas_preco where cod_cliente = 1");
            $resultado1->execute();
            $id1 = $resultado1->fetch(PDO::FETCH_ASSOC);
            $sql = $pdo ->prepare("select produtos.codigo, produtos.descricao as DESC,produtos.PCT_IPI,produtos.cod_grupo, grupos_produto.descricao, listas_itens.preco "
                    . "from listas_itens "
                    . "inner join produtos on (listas_itens.cod_produto = produtos.codigo) "
                    . "inner join grupos_produto on (produtos.cod_grupo = grupos_produto.codigo) "
                    . "where produtos.codigo=? and (listas_itens.id_lista ='".$id1['ID']."') and 
                        ((produtos.cod_grupo = 30 or produtos.cod_grupo = 1 or produtos.cod_grupo = 7)
                        or
                        (produtos.cod_grupo = 10 and produtos.codigo between 9000 and 9999))"
                    . "and produtos.codigo != 8025 and produtos.codigo != 6077 and produtos.codigo != 8003 "
                    . "and produtos.codigo != 6081 and produtos.codigo != 6082 and produtos.codigo != 6083 ");
            
            $sql->execute(array($id));
            $resultado = $sql->fetch(PDO::FETCH_ASSOC);
//            if($resultado['PCT_IPI'] > 0){
//                $ipi = ($resultado['PCT_IPI'] * $resultado['PRECO'])/100;
//                $ipi += $resultado['PRECO'];
//             }else{
//                $ipi = $resultado['PRECO'];
//             }
             
            //contruir variavel de retorno
             if($resultado['PCT_IPI'] > 0 ){
                $retorno[$indice]['ipi'] = $resultado['PCT_IPI'];
            } else{
                $retorno[$indice]['ipi'] = '';
            }
            $retorno[$indice]['codigo'] = $resultado['CODIGO'];
            $retorno[$indice]['titulo'] = $resultado['DESC'];            
            $retorno[$indice]['preco'] = $resultado['PRECO'];
            $retorno[$indice]['qtd'] = $qtd;
            $retorno[$indice]['subtotal'] = $resultado['PRECO'] * $qtd;
            $retorno[$indice]['tamanho'] = '';
           
            if($tam > 0){
                $sql_tam = $pdo->prepare("select PRODUTOS.CODIGO,PRODUTOS.COD_GRADE, GRADES_NROS.ID_NROGRADE,GRADES_NROS.COD_GRAD, GRADES_NROS.NUMERO "
                        . "FROM GRADES_NROS "
                        . "INNER JOIN PRODUTOS ON (GRADES_NROS.COD_GRAD = PRODUTOS.COD_GRADE)"
                        . "where GRADES_NROS.ID_NROGRADE=?");
                $sql_tam->execute(array($tam));
                $result = $sql_tam->fetch(PDO::FETCH_ASSOC);
                $retorno[$indice]['tamanho'] = $result['NUMERO'];
            }
        }
        return $retorno;
    }
    
    public function valorTotal(){
        $produtos = $this->listarProdutos();
        $total = 0;
        foreach ($produtos as $indice => $linha){
            $total += $linha['subtotal'];
        }
        return $total;
    }
}

final class CarrinhoSugestao {
    //put your code here
    public function __construct() {
        if(!isset($_SESSION['carrinho_sugestao'])){
            $_SESSION['carrinho_sugestao'] = array();
        }
    }
    
    public function adicionarProdutosSugestao($id, $qtd = 1, $tam = null){
        if(is_null($tam)){
            $indice = sprintf("%s:%s", (int)$id, 0);
        }else{
            $indice = sprintf("%s:%s", (int)$id,(int)$tam);
        }
        if(!isset($_SESSION['carrinho_sugestao'][$indice])){
            $_SESSION['carrinho_sugestao'][$indice] = (int)$qtd;
        }
    }
    
    
    public function alterarQtdSugestao($indice, $qtd){
        if(isset($_SESSION['carrinho_sugestao'][$indice])){
            if($qtd>0){
                $_SESSION['carrinho_sugestao'][$indice] = (int)$qtd;
            }
        }
    }
    
    public function excluirProdutoSugestao($indice){
        unset($_SESSION['carrinho_sugestao'][$indice]);
    }
    
    public function listarProdutosSugestao(){
        $retorno = array();
        foreach ($_SESSION['carrinho_sugestao'] as $indice => $qtd){
            //separar o id do produto do id do tamanho
            list($id, $tam) = explode(":", $indice);
            $pdo = conecta();
            $resultado1 = $pdo ->prepare("SELECT max(ID_LISTA)AS ID FROM listas_preco where cod_cliente = 1");
            $resultado1->execute();
            $id1 = $resultado1->fetch(PDO::FETCH_ASSOC);
            $sql = $pdo ->prepare("select produtos.codigo, produtos.descricao as DESC,produtos.PCT_IPI,produtos.cod_grupo, grupos_produto.descricao, listas_itens.preco "
                    . "from listas_itens "
                    . "inner join produtos on (listas_itens.cod_produto = produtos.codigo) "
                    . "inner join grupos_produto on (produtos.cod_grupo = grupos_produto.codigo) "
                    . "where produtos.codigo=? and (listas_itens.id_lista ='".$id1['ID']."') and 
                        ((produtos.cod_grupo = 30 or produtos.cod_grupo = 1 or produtos.cod_grupo = 7)
                        or
                        (produtos.cod_grupo = 10 and produtos.codigo between 9000 and 9999))"
                    . "and produtos.codigo != 8025 and produtos.codigo != 6077 and produtos.codigo != 8003 "
                    . "and produtos.codigo != 6081 and produtos.codigo != 6082 and produtos.codigo != 6083 ");
            $sql->execute(array($id));
            $resultado = $sql->fetch(PDO::FETCH_ASSOC);
//            if($resultado['PCT_IPI'] > 0){
//                $ipi = ($resultado['PCT_IPI'] * $resultado['PRECO'])/100;
//                $ipi += $resultado['PRECO'];
//             }else{
//                $ipi = $resultado['PRECO'];
//             }
            //contruir variavel de retorno
            $retorno[$indice]['codigo'] = $resultado['CODIGO'];
            $retorno[$indice]['titulo'] = $resultado['DESC'];
            $retorno[$indice]['preco'] = $resultado['PRECO'];
            $retorno[$indice]['qtd'] = $qtd;
            $retorno[$indice]['subtotal'] = $resultado['PRECO'] * $qtd;
            $retorno[$indice]['tamanho'] = '';
            
            if($tam > 0){
                $sql_tam = $pdo->prepare("select PRODUTOS.CODIGO,PRODUTOS.COD_GRADE, GRADES_NROS.ID_NROGRADE,GRADES_NROS.COD_GRAD, GRADES_NROS.NUMERO "
                        . "FROM GRADES_NROS "
                        . "INNER JOIN PRODUTOS ON (GRADES_NROS.COD_GRAD = PRODUTOS.COD_GRADE)"
                        . "where GRADES_NROS.ID_NROGRADE=?");
                $sql_tam->execute(array($tam));
                $result = $sql_tam->fetch(PDO::FETCH_ASSOC);
                $retorno[$indice]['tamanho'] = $result['NUMERO'];
            }
        }
        return $retorno;
    }
    
    
}
