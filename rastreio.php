<?php
if (isset($_GET['rastrear'])) {
    require_once $_SERVER["DOCUMENT_ROOT"] . '/farma/classes/conexao.php';
    $pdo = conecta();
    $nota = $_GET['nota'];
    $cliente = $_GET['cliente'];
    $consultar = $pdo->prepare("select notas.nro_nota,(transportadoras.codigo)as transportadora,"
            . "notas.cod_cliente,REPLACE(REPLACE(REPLACE(clientes.cgc, '/', ''), '-', ''), '.', '')as cgc "
            . "from transportadoras "
            . "inner join notas on (notas.cod_transportador = transportadoras.codigo) "
            . "inner join clientes on (notas.cod_cliente=clientes.codigo) "
            . "where transportadoras.ativo='S' and transportadoras.codigo != 0 and notas.cod_cliente = " . $cliente . " and notas.nro_nota=" . $nota);
    $consultar->execute();
    $linha = $consultar->fetch(PDO::FETCH_ASSOC);
    if (trim($linha['TRANSPORTADORA']) == 4) {
        echo header("Location: https://www.braspress.com.br/w/tracking/search?cnpj=".$linha["CGC"]."&numero=$nota&documentType=NOTAFISCAL");
    } else if (trim($linha['TRANSPORTADORA']) == 131) {
        echo header("Location: http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=72559297000130&nota=$nota");
    } else if (trim($linha['TRANSPORTADORA']) == 179) {
        echo header("Location: http://www.jadlog.com.br/trackingcorp.jsp?cnpj=72559297000130&nota=$nota");
    } else if (trim($linha['TRANSPORTADORA']) == 27 || 48 || 146) {
        $consultar2 = $pdo->prepare("select notas.nro_nota,conhecimentos.conhecimento,(transportadoras.codigo)as transportadora,"
            . "notas.cod_cliente,REPLACE(REPLACE(REPLACE(clientes.cgc, '/', ''), '-', ''), '.', '')as cgc "
            . "from transportadoras "
            . "inner join notas on (notas.cod_transportador = transportadoras.codigo) "
            . "inner join conhecimentos on (notas.nota = conhecimentos.nf)"
            . "inner join clientes on (notas.cod_cliente=clientes.codigo) "
            . "where transportadoras.ativo='S' and transportadoras.codigo != 0 and notas.cod_cliente = " . $cliente . " and notas.nro_nota=" . $nota);
        $consultar2->execute();
        $linha2 = $consultar2->fetch(PDO::FETCH_ASSOC);
        echo "<script>function load(){
            document.frm1.submit()
            }
            </script>
            <body onload=\"load()\">
            <form method=\"post\" action=\"https://www2.correios.com.br/sistemas/rastreamento/resultado_semcontent.cfm?objetos=\" id=\"frm1\" name=\"frm1\" >
            <input type=\"hidden\" name=\"objetos\" value=".$linha2["CONHECIMENTO"]." />"
        . "</form>"
        . "</body>";
    } else {
        echo "Erro!";
    }
}
?>


