<?php 
session_start();
require_once ('classes/conexao.php');
date_default_timezone_set('America/Sao_Paulo');
$datahora = date('d.m.Y H:i');
$data = date('d.m.Y');

$pdo = conecta_mysql();

$cliente = $_POST['cliente'];
$frete = $_POST['frete'];
$refCode = $_POST['refCode'];

$codigo = $_POST['codigo'];
$array = explode(",", $codigo);

$grade = $_POST['grade'];
$array2 = explode(",", $grade);

$ipi = $_POST['ipi'];
$array3 = explode(",", $ipi);

$preco = $_POST['preco'];
$array4 = explode(",", $preco);

$qtd = $_POST['qtd'];
$array5 = explode(",", $qtd);

$desconto = $_POST['desconto'];
$array6 = explode(",", $desconto);

$st = $_POST['st'];
$array7 = explode(",", $st);

if(empty($_POST['ckb'])){
    $ckb = '.';
//      $ckb = 'Este pedido é um teste. Não liberar para faturamento';
}else{
    $ckb = $_POST['ckb'];
//    $ckb = 'Este pedido é um teste. Não liberar para faturamento';
}

if(isset($_POST['dados_cliente']) == "retorna_dados_cliente"){
    $retorna = $pdo->prepare("select * from app_sopes_clientes where id=". $cliente);
    $retorna->execute();
    $linha_retorno = $retorna->fetch(PDO::FETCH_ASSOC);
    echo json_encode($linha_retorno);
}

$zero = "0";
$n = "N";

 try {
     
 $sql2 = ("update app_sopes_pedidos set emissao=:emissao, entrega=:entrega, cond1=:cond1, cond2=:cond2, "
         . "cond3=:cond3, cond4=:cond4, cond5=:cond5, cond6=:cond6, cond7=:cond7, cond8=:cond8, cond9=:cond9, "
         . "cond10=:cond10, cond11=:cond11, cond12=:cond12, valor_frete=:valor_frete, status=:status, ckb=:ckb where id=".$refCode);
        $stmt = $pdo->prepare($sql2);
        $stmt->bindParam(':emissao', $datahora, PDO::PARAM_STR);//S
        $stmt->bindParam(':entrega', $data, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond1', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond2', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond3', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond4', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond5', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond6', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond7', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond8', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond9', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond10', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond11', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':cond12', $zero, PDO::PARAM_STR);//S
        $stmt->bindParam(':valor_frete', $frete, PDO::PARAM_STR);//S
        $stmt->bindParam(':status', $n, PDO::PARAM_STR);//S
        $stmt->bindParam(':ckb', $ckb, PDO::PARAM_STR);//S
        $stmt->execute();

        for($i = 0; $i < count($array);$i ++){

$sql3 = ("insert into app_sopes_pedidos_item (id_pedido, cod_produto, grade, qtd, unitario, ipi, desconto, personalizado, aplicou_acrescimo,valor_icms_subst_trib) "
                                 . "values (:id_pedido, :cod_produto, :grade, :qtd, :unitario, :ipi, :desconto, :personalizado, :aplicou_acrescimo, :valor_icms_subst_trib)");
       $stmt2 = $pdo->prepare($sql3);
       $stmt2->bindParam(':id_pedido', $refCode, PDO::PARAM_STR);
       $stmt2->bindParam(':cod_produto', $array[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':grade',  $array2[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':qtd',  $array5[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':unitario', $array4[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':ipi', $array3[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':desconto', $array6[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':personalizado', $n, PDO::PARAM_STR);
       $stmt2->bindParam(':aplicou_acrescimo', $n, PDO::PARAM_STR);
       $stmt2->bindParam(':valor_icms_subst_trib', $array7[$i], PDO::PARAM_STR);
       $stmt2->execute();
}
} catch (PDOException $e) {
        echo $e->getMessage();
}

    

