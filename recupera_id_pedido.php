<?php 
session_start();
require_once ('classes/conexao.php');
date_default_timezone_set('America/Sao_Paulo');


if(isset($_POST['finaliza']) == "finalina"){
    $pdo = conecta_mysql();
    $cod_cliente = $_POST['cliente'];
    $id_pedidos = $pdo->prepare("insert into app_sopes_pedidos (cod_cliente) "
            . "values (:cod_cliente)");
    $id_pedidos->bindParam(':cod_cliente', $cod_cliente, PDO::PARAM_INT);
    $id_pedidos->execute();
    
    $id = $pdo->lastInsertId();
    echo $id;
}