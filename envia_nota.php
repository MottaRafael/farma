 <?php
// Inclui o arquivo class.phpmailer.php localizado na pasta class
require_once("PHPMailer/class.phpmailer.php");
 
// Inicia a classe PHPMailer
$mail = new PHPMailer(true);
 
// Define os dados do servidor e tipo de conexão
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsSMTP(); // Define que a mensagem será SMTP
 
try {
     $mail->Host = 'smtp.netwizard.com.br'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
     $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
     $mail->Port       = 587; //  Usar 587 porta SMTP
     $mail->Username = 'atendimento@sopes.com.br'; // Usuário do servidor SMTP (endereço de email)
     $mail->Password = 'qubre5up'; // Senha do servidor SMTP (senha do email usado)
 
     //Define o remetente
     // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=    
     $mail->SetFrom('atendimento@sopes.com.br', 'Atendimento'); //Seu e-mail
     //$mail->AddReplyTo('seu@e-mail.com.br', 'Nome'); //Seu e-mail
     $mail->Subject = 'Nota';//Assunto do e-mail
 
     //Define os destinatário(s)
     
     $mail->AddAddress($_POST['email'], $_POST['email']);
 
     //Campos abaixo são opcionais 
     //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
     //$mail->AddCC('destinarario@dominio.com.br', 'Destinatario'); // Copia
     //$mail->AddBCC('destinatario_oculto@dominio.com.br', 'Destinatario2`'); // Cópia Oculta
     //$mail->AddAttachment('images/phpmailer.gif');      // Adicionar um anexo
  
     //Define o corpo do email
     
   if (isset($_POST['acao'])&&( $_POST['acao']== 'pdf')) {
       
    $arquivo = $_POST['nota'] . ".pdf";
    //$path = '\\\10.0.0.140\PDFPalterm\Nota_'.$arquivo;
    $path = '/mnt/pdfpalterm/Nota_'.$arquivo;
    $mail->MsgHTML('Segue PDF em anexo.'); 
    $fileName = basename($path);
    $mail->AddAttachment($path);
    
    }
    
    if (isset($_POST['acao'])&&( $_POST['acao']== 'xml')) {
        
    $arquivo = $_POST['nota'] . ".xml";
    //$path = '\\\10.0.0.140\NFEPalterm\NFE'.$arquivo;
    $path = '/mnt/nfepalterm/NFE'.$arquivo;
    $mail->MsgHTML('Segue XML em anexo.'); 
    $fileName = basename($path);
    $mail->AddAttachment($path);
    
    }
    
     //Caso queira colocar o conteudo de um arquivo utilize o método abaixo ao invés da mensagem no corpo do e-mail.
    //$mail->MsgHTML(file_get_contents($path));

     $mail->Send();
     echo "Mensagem enviada com sucesso\n";
 
    //caso apresente algum erro é apresentado abaixo com essa exceção.
    } catch (phpmailerException $e) {
      echo $e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
}