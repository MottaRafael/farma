<?php
session_start();
require_once ('classes/Carrinho.class.php');
require 'crud.php';
require_once ('classes/conexao.php');

$pdo= conecta();


if(isset($_GET['codigo'])){
$teste2 = $_GET['codigo'];
$tes = $pdo->prepare("select EMAIL_APP from CLIENTES where CODIGO=".$teste2."");
$tes->execute();
$testando = $tes->fetch(PDO::FETCH_ASSOC);

$idCustomer = $teste2;
}elseif(isset($_POST['cod_razao'])){
    $cod_razao = $_POST['cod_razao'];
    $_SESSION['razao_p'] = $cod_razao;
    $idCustomer = $_SESSION['razao_p'];
}

$carrinho = new Carrinho();

if(isset($_POST['acao'])&&( $_POST['acao']== 'add')){
    $id_produto = (int)$_POST['id'];
    $qtd = (int)$_POST['qtd'];
    $tam = (isset($_POST['tamanho']))? $_POST['tamanho']: null;
    $carrinho->adicionarProdutos($id_produto, $qtd, $tam);
}

if(isset($_POST['atualizar'])){
    $qtd = $_POST['qtd'];
    foreach ($qtd as $indice => $valor){
        $carrinho->alterarQtd($indice, $valor);
    }
}

if(isset($_GET['acao']) && $_GET['acao']== 'del'){
    $id = $_GET['id'];
    $carrinho->excluirProduto($id);
}

$produtos = $carrinho->listarProdutos();
$total = $carrinho->valorTotal();

$teste = $pdo->prepare('SELECT GEN_ID(gen_pedidos_pre_id, 1) FROM RDB$DATABASE');
$teste->execute();
$proximo = $teste->fetch(PDO::FETCH_ASSOC);

$sql = $pdo->prepare("Select UFFATURAMENTO from clientes where codigo=".$idCustomer);
$sql->execute();
$uf = $sql->fetch(PDO::FETCH_ASSOC);


$resultado = $pdo->prepare("SELECT max(ID_LISTA)AS ID FROM listas_preco where cod_cliente = 1");
$resultado->execute();
$id = $resultado->fetch(PDO::FETCH_ASSOC);

$result = $pdo->prepare("select produtos.codigo, produtos.descricao as DESC,produtos.DESCRICAO_COMPLETA, PRODUTOS.PCT_IPI, 
                        produtos.cod_grupo, grupos_produto.descricao, listas_itens.preco
                        from listas_itens
                        inner join produtos on (listas_itens.cod_produto = produtos.codigo)
                        inner join grupos_produto on (produtos.cod_grupo = grupos_produto.codigo)
                        where 
                        produtos.ativo = 'S' and
                        (listas_itens.id_lista = '". $id['ID']."') and
                        ((produtos.cod_grupo = 30 or produtos.cod_grupo = 1 or produtos.cod_grupo = 7)
                        or
                        (produtos.cod_grupo = 10 and produtos.codigo between 9000 and 9999)) and produtos.codigo != 8025
                        and CHAR_LENGTH(produtos.codigo) <= 4 and produtos.codigo != 787 and produtos.codigo != 786 and 
                        produtos.codigo != 6077 and produtos.codigo != 8003 and produtos.codigo != 3548 and
                        produtos.codigo != 6081 and produtos.codigo != 6082 and produtos.codigo != 634 and
                        produtos.codigo != 6083 and listas_itens.preco > 1 order by grupos_produto.descricao desc");
$result->execute();
$linha = $result->fetch(PDO::FETCH_ASSOC);

 if(empty($_SESSION['razao_p'])){
   $cli = $idCustomer;
 }else{
   $cli = $_SESSION['razao_p'];
 }

 $nome = $pdo->prepare("select nome, cgc from clientes where codigo=". $cli);
 $nome->execute();
 $nome_linha = $nome->fetch(PDO::FETCH_ASSOC);
 
?>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,  maximum-scale=1.0, minimum-scale=1.0">
		<title>Pedidos</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
                <title>Carrinho</title>
                <style>
                    #interna{
                       text-align: center;
                       font-size: 9;
                       font-family: Avant Garde,Avantgarde,Century Gothic,CenturyGothic,AppleGothic,sans-serif;  ;
                    }
                    #img{
                        height: 60;
                        widht: 45;
                    }
                    #div{
                        display: flex;
                        aling-items: center;
                        justify-content: space-between;
                        margin-top: 40;
                    }
                    input[type='number'] {
                        -moz-appearance: textfield;
                    }
                    input::-webkit-outer-spin-button,
                    input::-webkit-inner-spin-button {
                        -webkit-appearance: none;
                    }
                    .btn-prima {
                    color: #fff;
                    background-color: #00B4BD;
                    border-color: #00B4BD;
                    }
                    .btn-prima:focus,
                    .btn-prima.focus {
                      color: #fff;
                      background-color: #00B4BD;
                      border-color: #00B4BD;
                    }
                    .btn-prima:hover {
                      color: #fff;
                      background-color: #00B4BD;
                      border-color: #00B4BD;
                    }
                    .btn-prima:active,
                    .btn-prima.active,
                    .open > .dropdown-toggle.btn-prima {
                      color: #fff;
                      background-color: #00B4BD;
                      border-color: #00B4BD;
                    }
                    .btn-prima:active:hover,
                    .btn-prima.active:hover,
                    .open > .dropdown-toggle.btn-prima:hover,
                    .btn-prima:active:focus,
                    .btn-prima.active:focus,
                    .open > .dropdown-toggle.btn-prima:focus,
                    .btn-prima:active.focus,
                    .btn-prima.active.focus,
                    .open > .dropdown-toggle.btn-prima.focus {
                      color: #fff;
                      background-color: #00B4BD;
                      border-color: #00B4BD;
                    }
                    .btn-prima:active,
                    .btn-prima.active,
                    .open > .dropdown-toggle.btn-prima {
                      background-image: none;
                    }
                    .btn-prima.disabled:hover,
                    .btn-prima[disabled]:hover,
                    fieldset[disabled] .btn-prima:hover,
                    .btn-prima.disabled:focus,
                    .btn-prima[disabled]:focus,
                    fieldset[disabled] .btn-prima:focus,
                    .btn-prima.disabled.focus,
                    .btn-prima[disabled].focus,
                    fieldset[disabled] .btn-prima.focus {
                      background-color: #00B4BD;
                      border-color: #00B4BD;
                    }
                    .btn-prima .badge {
                      color: #00B4BD;
                      background-color: #fff;
                    }

                    .btn-primar {
                      color: #fff;
                      background-color: #008B8B;
                      border-color: #008B8B;
                    }
                    .btn-primar:focus,
                    .btn-primar.focus {
                      color: #fff;
                      background-color: #008B8B;
                      border-color: #008B8B;
                    }
                    .btn-primar:hover {
                      color: #fff;
                      background-color: #008B8B;
                      border-color: #008B8B;
                    }
                    .btn-primar:active,
                    .btn-primar.active,
                    .open > .dropdown-toggle.btn-primar {
                      color: #fff;
                      background-color: #008B8B;
                      border-color: #008B8B;
                    }
                    .btn-primar:active:hover,
                    .btn-primar.active:hover,
                    .open > .dropdown-toggle.btn-primar:hover,
                    .btn-primar:active:focus,
                    .btn-primar.active:focus,
                    .open > .dropdown-toggle.btn-primar:focus,
                    .btn-primar:active.focus,
                    .btn-primar.active.focus,
                    .open > .dropdown-toggle.btn-primar.focus {
                      color: #fff;
                      background-color: #008B8B;
                      border-color: #008B8B;
                    }
                    .btn-primar:active,
                    .btn-primar.active,
                    .open > .dropdown-toggle.btn-primar {
                      background-image: none;
                    }
                    .btn-primar.disabled:hover,
                    .btn-primar[disabled]:hover,
                    fieldset[disabled] .btn-primar:hover,
                    .btn-primar.disabled:focus,
                    .btn-primar[disabled]:focus,
                    fieldset[disabled] .btn-primar:focus,
                    .btn-primar.disabled.focus,
                    .btn-primar[disabled].focus,
                    fieldset[disabled] .btn-primar.focus {
                      background-color: #008B8B;
                      border-color: #008B8B;
                    }
                    .btn-primar .badge {
                      color: #008B8B;
                      background-color: #fff;
                    }
                     .modal {
                text-align: center;
                position: fixed;
            }
            
            .modal:before {
                content: '';
                display: inline-block;
                height: 100%;
                vertical-align: middle;
                margin-right: -4px;
            }
            
            .modal-dialog {
                display: inline-block;
                text-align: left;
            }
                </style>
    </head>
    <body style="margin-left: -30px;margin-right: -30px; background-color:#00B4BD;">
        <div class="container theme-showcase" role="main">
             <div class="col-lg-12">
                 <br/>
         <div class="col-lg-4">
        <table class="table table-striped"  style="background-color:#fff;" id="interna">
            <caption><label>Carrinho de Compras</label></caption>
        <thead>
            <tr>
                <th colspan="8">
                    <?php echo "Razão: " . $nome_linha['NOME']; ?>
                </th>
            </tr>
            <tr>
                <th colspan="8">
                    <?php echo "CNPJ: " . $nome_linha['CGC']; ?>
                </th>
            </tr>
        </thead>
            <form action="?acao=up&codigo=<?php echo $teste2; ?>&desconto=S" method="POST">
        <tfoot>
            <td colspan="8"><a href="index.php?email=<?php echo $testando['EMAIL_APP']; ?>" ><p style="font-size:16"><span class="glyphicon glyphicon-shopping-cart" ></span> Continuar Comprando</p></a></td>
        </tfoot>
                <tbody>
     <?php
     $contar = count($produtos);
     
              if (($total < '200.0' && $uf['UFFATURAMENTO'] == 'RS') ||  
              ($total < '200.0' && $uf['UFFATURAMENTO'] == 'SC') ||  
              ($total < '200.0' && $uf['UFFATURAMENTO'] == 'PR') ||
              ($total < '200.0' && $uf['UFFATURAMENTO'] == 'SP') ||  
              ($total < '200.0' && $uf['UFFATURAMENTO'] == 'RJ') ||  
              ($total < '200.0' && $uf['UFFATURAMENTO'] == 'ES') ||  
              ($total < '200.0' && $uf['UFFATURAMENTO'] == 'MG') ||  
              ($total < '200.0' && $uf['UFFATURAMENTO'] == 'MS') ||  
              ($total < '200.0' && $uf['UFFATURAMENTO'] == 'GO') ||  
              ($total < '200.0' && $uf['UFFATURAMENTO'] == 'DF') ||
              ($total < '200.0' && $uf['UFFATURAMENTO'] == 'MT')) {
               $frete = '10'; 
              } elseif (($total < '250.0' && $uf['UFFATURAMENTO'] == 'MA') ||
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'PI') ||  
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'CE') ||
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'RN') ||  
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'PB') ||  
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'PE') ||  
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'AL') ||  
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'SE') ||  
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'BA')) {
                 $frete = '15'; 
              } elseif (($total < '250.0' && $uf['UFFATURAMENTO'] == 'RO') ||
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'AC') ||  
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'AM') ||
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'RR') ||  
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'AP') ||  
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'PA') ||  
              ($total < '250.0' && $uf['UFFATURAMENTO'] == 'TO')) {
                   $frete = '20'; 
              } else {
                  $frete = '0'; 
              }
              
     if ($contar == 0) {
          echo '<tr>
                    <td colspan="8">Não há produto no carrinho</td>
                </tr>';
     } else {
         $i = 0;
         $x = 0;
         $y = 0;
         $z = 0;
         $j = 0;
     foreach ($produtos as $indice => $produto): ?>
            <tr>
                <td colspan="1" style="vertical-align:middle;" align="center">
                <img id="img" src="imagens/<?php echo $produto['codigo']; ?>.png">
                    <input type="number"  class="form-control" style="width:70;margin-top: 10" name="qtd[<?php echo $indice; ?>]" value="<?php echo $produto['qtd']; ?>" />
                </td>
                <td colspan="6"><?php echo $produto['titulo'].' - '.$produto['tamanho'];?>
                    <div id="div">
                        <label style="text-align:left;">Unitário <br/>R$ <?php echo number_format($produto['preco'], 2, ',', '.'); ?>
                            <br/>
                            <br/>
                         <sup>
                             <?php
//                              var_dump($produto);
                             $calcula_ipi = $pdo->prepare("select cod_natureza_dentro_estado, cod_natureza_fora_estado,seq_fora_estado,seq_dentro_estado from produtos where codigo=".$produto['codigo']);
                             $calcula_ipi->execute(); 
                             $resolve = $calcula_ipi->fetch(PDO::FETCH_ASSOC);
                             
                             if (!empty($resolve['COD_NATUREZA_DENTRO_ESTADO'])||(!empty($resolve['COD_NATUREZA_DENTRO_ESTADO']))) {
                                         $calcular = $pdo->prepare("select calcula_ipi from naturezas_operacao where cod_natureza=" . $resolve['COD_NATUREZA_DENTRO_ESTADO']." and seq=".$resolve['SEQ_DENTRO_ESTADO']);
                                         $calcular->execute();
                                         $calculou = $calcular->fetch(PDO::FETCH_ASSOC);

                                         $calcular2 = $pdo->prepare("select calcula_ipi from naturezas_operacao where cod_natureza=" . $resolve['COD_NATUREZA_FORA_ESTADO']. "and seq=".$resolve['SEQ_FORA_ESTADO']);
                                         $calcular2->execute();
                                         $calculou2 = $calcular2->fetch(PDO::FETCH_ASSOC);

                                     } else {
                                         $calculou['CALCULA_IPI'] = 'S';
                                         $calculou2['CALCULA_IPI'] = 'S';
                                     }

                                     if(($produto['ipi'] != '')&&((trim($calculou['CALCULA_IPI']) == "S")||((trim($calculou2['CALCULA_IPI']) == "S")))) {
                                    echo '+ '. number_format($produto['ipi'], 0, ',', '.') .'% IPI';
                                } else {
                                    echo '';
                                }
                                
                             ?>
                         </sup>
                         <sub style="position:relative;left: -35px;bottom: -5px;">
                            <?php 
                            
                            if(isset($_GET['desconto'])||(isset($_POST['desconto']))){
                                $desc = $_GET['desconto'];
                                if($desc == "S") {
                                    
                                    $desconto = 3;
                                    $desc = "S";
                                    if(($produto['codigo'] == 3388)||($produto['codigo'] == 3387)){
                                        $desconto = 0;
                                        echo "<input type='hidden' class='get_value_desconto' id='desconto' value='".number_format($desconto, 2, '.', '.')."'/>";
                                    } else {
                                            $desconto = 3;                            
                                        echo "<input type='hidden' class='get_value_desconto' id='desconto' value='".number_format($desconto, 2, '.', '.')."'/>";
                                    }
                                    
                                 } elseif ($desc == "N") {
                                     
                                     $desc = "N";
                                    $desconto=0;
                                    echo "<input type='hidden' class='get_value_desconto' id='desconto' value='".number_format($desconto, 2, '.', '.')."'/>";
                                 }elseif ($desc == "NN") {
                                     
                                     $desc = "NN";
                                    $desconto=0;
                                    echo "<input type='hidden' class='get_value_desconto' id='desconto' value='".number_format($desconto, 2, '.', '.')."'/>";
                                 }
                             } else {
                                 $desconto = 0;
                                 echo "<input type='hidden' class='get_value_desconto' id='desconto' value='".number_format($desconto, 2, '.', '.')."'/>";
                             }
                             
                             
                             
                           
                                                      
                           if(($produto['codigo'] != 3388)&&($produto['codigo'] != 3387)){
                               $calculo_desconto = ($produto['subtotal'] * $desconto) / 100;
                               $j += $calculo_desconto;
                           }
                           
                           $freteItem = (($frete * (($produto['preco']*100)/($total))/100)*$produto['qtd']);
                           
                           
                                $valor_ST = ValorSubst($idCustomer, $produto['codigo'], $produto['preco'], $produto['qtd'], $freteItem, $desconto);
                               
                                if($valor_ST != '') {
                                    $z += $valor_ST;
                                   echo "+ ST R$ " . number_format($valor_ST, 2, ',', '.') ."*";
                                   echo "<input type='hidden' class='get_value_st' id='st' value='".number_format($valor_ST, 2, '.', '.')."'/>";
                                } else {
                                    $valor_ST = 0;
                                    echo "<input type='hidden' class='get_value_st' id='st' value='".$valor_ST."'/>";
                                }
                                
                             if (($produto['ipi'] > 0)&&(($produto['codigo'] != 3388)&&($produto['codigo'] != 3387))) {
                                
                                $ipi = (((($produto['subtotal'] - $calculo_desconto )+ $freteItem)*$produto['ipi'])/100);
                                
                                $subtotal = ($ipi + $produto['subtotal']) + $valor_ST ;
                                
                                $total_com_ipi =  $subtotal;
                                
                                $i += $total_com_ipi;
                                
                                $y += $ipi;
                             
                            } else {
                                
                                $ipi = 0;
                                
                                $subtotal = ( $freteItem + $valor_ST + $produto['subtotal']);
                                
                                $total_final_sem_ipi = $produto['subtotal'] + $valor_ST + $ipi;  
                                
                                $x += $total_final_sem_ipi;
                            }
                            $_SESSION['qtd'] = array($produto['qtd']) 
                            ?>
                             
                          </sub>
                        </label>
                     <label style="color: #0c85d6;text-align:left;">SubTotal<br/> R$ <?php echo number_format(($produto['subtotal'] + $valor_ST + $ipi), 2, ',', '.')?></label>
                    </div>
                </td>
                <td colspan="2" style="vertical-align:middle;"><a href="<?php echo "carrinho.php?acao=del&id=".$indice."&codigo=".$teste2 ?>"><h4><span class="glyphicon glyphicon-trash"></span></h4></a>
                <hr>
                    <button class="btn btn-prima" name="atualizar"><span class="glyphicon glyphicon-refresh"></span></button>
                </td>
            </tr>
            
        
                    
                    
                    
            <?php
            $_SESSION['codigo'][] = $produto['codigo'];
            $_SESSION['titulo'][] = $produto['titulo'];
            $_SESSION['preco'][] = number_format($produto['preco'], 2, '.', '.');
            endforeach;  
            
            $y_ipi = $y;
            
            $total_final = $i + $x;
            
            $total_final2 = ($total * $desconto) / 100;
            
            if (isset($_GET['desconto']) || (isset($_POST['desconto']))) {
                $desc = $_GET['desconto'];
                if ($desc == "S") {
                    $desconto2 = 3;
                    $desc = "S";
                } elseif ($desc == "N") {
                    $desc = "N";
                    $desconto2 = 0;
                } elseif ($desc == "NN") {
                    $desc = "NN";
                    $desconto2 = 0;
                }
            } else {
                $desconto2 = 0;
            }
        ?>
                    <tr>
                        <td colspan="5"><label>Valor dos Produtos</label></td>
                        <td colspan="3"><label>R$ <?php echo number_format($total, 2, ',', '.'); ?></label></td>
                    </tr>
                    <tr>
                        <td colspan="5"><label>IPI</label></td>
                        <td colspan="3"><label>R$ <?php echo number_format($y_ipi, 2, ',', '.'); ?></label></td>
                    </tr>
                    <?php $_SESSION['ipi'][] = $y_ipi ?>
                    <tr>
                        <td colspan="5"><label>*Subst. Trib.</label></td>
                        <td colspan="3"><label>R$ <?php echo number_format($z, 2, ',', '.'); ?></label></td>
                    </tr>
                    <tr>
                        <td colspan="5"><label>Desconto</label></td>
                        <td colspan="3"><label><?php echo number_format($desconto2, 0, ',', '.'); ?> %</label></td>
                    </tr>
                    <tr>
                        <td colspan="5"><label>Valor Desconto</label></td>
                        <td colspan="3"><label>R$ <?php echo number_format($j, 2, ',', '.'); ?></label></td>
                    </tr>
            <?php 
            
            if($frete == '0'){
                
                echo '<tr>
                        <td colspan="5"><label style="color:#259127">Frete Grátis</label></td>
                        <td colspan="3"><label style="color:#259127">R$ '. number_format($frete, 2, ',', '.') . '</label></td>
                    </tr>';
                
            } else {
                
                echo '<tr>
                        <td colspan="5"><label>Frete</label></td>
                        <td colspan="3"><label>R$ '. number_format($frete, 2, ',', '.') . '</label></td>
                    </tr>';
                
            }
            $tot_cartao = number_format((($total - ($j) + $frete)+ $z + $y_ipi), 2, '.', '.');
            $_SESSION['tot_cartao'] = $tot_cartao;
            ?>
                    
                    <tr>
                        <td colspan="5"><label>Total</label></td>
                        <td colspan="3"><label>R$ <?php echo number_format((($total - ($j) + $frete)+ $z + $y_ipi), 2, ',', '.'); ?></label></td>
                    </tr>
                    
                    
             <?php 
             
                   if (($uf['UFFATURAMENTO'] == 'AC') ||
                                ($uf['UFFATURAMENTO'] == 'AM') ||
                                ($uf['UFFATURAMENTO'] == 'AP') ||
                                ($uf['UFFATURAMENTO'] == 'RR') ||
                                ($uf['UFFATURAMENTO'] == 'RO') ||
                                ($uf['UFFATURAMENTO'] == 'PA')) {

                            if ((($uf['UFFATURAMENTO'] == 'AC') ||
                                    ($uf['UFFATURAMENTO'] == 'AM') ||
                                    ($uf['UFFATURAMENTO'] == 'AP') ||
                                    ($uf['UFFATURAMENTO'] == 'RR') ||
                                    ($uf['UFFATURAMENTO'] == 'RO')) && ($total > '400.0') && ($total < '900.0')) {
                                ?>

                                <tr>
                                    <td colspan="5" style="vertical-align:middle;">
                                        <label>Condições de Pagamento no Boleto</label>
                                    </td>
                                    <td colspan="3">
                                        <label>
                                            <form>
                                                <div class="col-xs-10">
                                                    <input type="radio" style="vertical-align: middle" class="get_value_radio" value="30" id="cond" name="desconto"
                                                    <?php
                                                    if (!empty($desc) && ($desc == "S")) {
                                                        echo "checked";
                                                    }
                                                    ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond">
                                                        <a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=S" ?>"> 30 dias c/ desc.</a>
                                                    </label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="60" class="get_value_radio" id="cond2" name="desconto" 
                                                    <?php
                                                    if (!empty($desc) && ($desc == "N")) {
                                                        echo "checked";
                                                    }
                                                    ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" >
                                                        <a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=N" ?>"> 60 dias direto</a>
                                                    </label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="45-75" class="get_value_radio" id="cond3" name="desconto" 
                                                    <?php
                                                    if (!empty($desc) && ($desc == "NN")) {
                                                        echo "checked";
                                                    }
                                                    ?>
                                                           disabled="true" />
                                                    <label for="cond2" >
                                                        <a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=NN" ?>"> 45/75 dias</a>
                                                    </label>
                                                </div>
                                            </form>
                                        </label>
                                    </td>
                                </tr>
                                <?php
                            } elseif ((($uf['UFFATURAMENTO'] == 'AC') ||
                                    ($uf['UFFATURAMENTO'] == 'AM') ||
                                    ($uf['UFFATURAMENTO'] == 'AP') ||
                                    ($uf['UFFATURAMENTO'] == 'RR') ||
                                    ($uf['UFFATURAMENTO'] == 'RO')) && ($total > '900.0')) {
                                ?>

                                <tr>
                                    <td colspan="5" style="vertical-align:middle;"><label>Condições de Pagamento no Boleto</label></td>
                                    <td colspan="3">
                                        <label>
                                            <form>
                                                <div class="col-xs-10">
                                                    <input type="radio" style="vertical-align: middle" value="30" class="get_value_radio" id="cond" name="desconto"
                                                    <?php
                                                    if (!empty($desc) && ($desc == "S")) {
                                                        echo "checked";
                                                    }
                                                    ?>
                                                           disabled="true" />
                                                    <label for="cond"><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=S" ?>"> 30 dias c/ desc.</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="60" id="cond2" class="get_value_radio" name="desconto" 
                                                    <?php
                                                    if (!empty($desc) && ($desc == "N")) {
                                                        echo "checked";
                                                    }
                                                    ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=N" ?>"> 60 dias direto</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="45-75-105" id="cond3" class="get_value_radio" name="desconto" 
                                                    <?php
                                                    if (!empty($desc) && ($desc == "NN")) {
                                                        echo "checked";
                                                    }
                                                    
                                                    ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=NN" ?>"> 45/75/105 dias</a></label>
                                                </div>
                                            </form>
                                        </label>
                                    </td>
                                </tr>

                                <?php
                            } else {
                                ?>
                                <tr>
                                    <td colspan="5" style="vertical-align:middle;"><label>Condições de Pagamento no Boleto</label></td>
                                    <td colspan="3"><label>
                                            <form>
                                                <div class="col-xs-10">
                                                    <input type="radio" style="vertical-align: middle" value="30" class="get_value_radio" id="cond" name="desconto"
                                                        <?php
                                                        if (!empty($desc) && ($desc == "S")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond"><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=S" ?>"> 30 dias c/ desc.</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="60" class="get_value_radio" id="cond2" name="desconto" 
                                                        <?php
                                                        if (!empty($desc) && ($desc == "N")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=N" ?>"> 60 dias direto</a></label>
                                                </div>
                                            </form>
                                        </label></td>
                                </tr>
                                    <?php
                                }
                            } elseif (($uf['UFFATURAMENTO'] == 'AL') ||
                                    ($uf['UFFATURAMENTO'] == 'BA') ||
                                    ($uf['UFFATURAMENTO'] == 'CE') ||
                                    ($uf['UFFATURAMENTO'] == 'MA') ||
                                    ($uf['UFFATURAMENTO'] == 'PB') ||
                                    ($uf['UFFATURAMENTO'] == 'PE') ||
                                    ($uf['UFFATURAMENTO'] == 'PI') ||
                                    ($uf['UFFATURAMENTO'] == 'RN') ||
                                    ($uf['UFFATURAMENTO'] == 'SE') ||
                                    ($uf['UFFATURAMENTO'] == 'TO') ||
                                    ($uf['UFFATURAMENTO'] == 'MT')) {

                                if ((($uf['UFFATURAMENTO'] == 'AL') ||
                                        ($uf['UFFATURAMENTO'] == 'BA') ||
                                        ($uf['UFFATURAMENTO'] == 'CE') ||
                                        ($uf['UFFATURAMENTO'] == 'MA') ||
                                        ($uf['UFFATURAMENTO'] == 'PB') ||
                                        ($uf['UFFATURAMENTO'] == 'PE') ||
                                        ($uf['UFFATURAMENTO'] == 'PI') ||
                                        ($uf['UFFATURAMENTO'] == 'RN') ||
                                        ($uf['UFFATURAMENTO'] == 'SE') ||
                                        ($uf['UFFATURAMENTO'] == 'TO') ||
                                        ($uf['UFFATURAMENTO'] == 'MT')) && ($total > '400.0') && ($total < '900,0')) {
                                    ?>
                                <tr>
                                    <td colspan="5" style="vertical-align:middle;"><label>Condições de Pagamento no Boleto</label></td>
                                    <td colspan="3"><label>
                                            <form>
                                                <div class="col-xs-10">
                                                    <input type="radio" style="vertical-align: middle" value="20" class="get_value_radio" id="cond" name="desconto"
                                                        <?php
                                                        if (!empty($desc) && ($desc == "S")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond"><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=S" ?>"> 20 dias c/ desc.</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="45" id="cond2" class="get_value_radio" name="desconto" 
                                                        <?php
                                                        if (!empty($desc) && ($desc == "N")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=N" ?>"> 45 dias direto</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="" id="cond3" class="get_value_radio" name="desconto" 
                                                        <?php
                                                        if (!empty($desc) && ($desc == "NN")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=NN" ?>"> 30/60 dias</a></label>
                                                </div>
                                            </form>
                                        </label></td>
                                </tr>
                                        <?php
                                    } elseif ((($uf['UFFATURAMENTO'] == 'AL') ||
                                            ($uf['UFFATURAMENTO'] == 'BA') ||
                                            ($uf['UFFATURAMENTO'] == 'CE') ||
                                            ($uf['UFFATURAMENTO'] == 'MA') ||
                                            ($uf['UFFATURAMENTO'] == 'PB') ||
                                            ($uf['UFFATURAMENTO'] == 'PE') ||
                                            ($uf['UFFATURAMENTO'] == 'PI') ||
                                            ($uf['UFFATURAMENTO'] == 'RN') ||
                                            ($uf['UFFATURAMENTO'] == 'SE') ||
                                            ($uf['UFFATURAMENTO'] == 'TO') ||
                                            ($uf['UFFATURAMENTO'] == 'MT')) && ($total > '900,0')) {
                                        ?>
                                <tr>
                                    <td colspan="5" style="vertical-align:middle;"><label>Condições de Pagamento no Boleto</label></td>
                                    <td colspan="3"><label>
                                            <form>
                                                <div class="col-xs-10">
                                                    <input type="radio" style="vertical-align: middle" value="20" class="get_value_radio" id="cond" name="desconto"
                                                        <?php
                                                        if (!empty($desc) && ($desc == "S")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond"><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=S" ?>"> 20 dias dias c/ desc.</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="45" class="get_value_radio" id="cond2" name="desconto" 
                                                        <?php
                                                        if (!empty($desc) && ($desc == "N")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=N" ?>"> 45 dias direto</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="30-60-90" class="get_value_radio" id="cond3" name="desconto" 
                                                        <?php
                                                        if (!empty($desc) && ($desc == "NN")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=NN" ?>"> 30/60/90 dias</a></label>
                                                </div>
                                            </form>
                                        </label></td>
                                </tr>
                                    <?php
                                } else {
                                    ?>
                                <tr>
                                    <td colspan="5" style="vertical-align:middle;"><label>Condições de Pagamento no Boleto</label></td>
                                    <td colspan="3"><label>
                                            <form>
                                                <div class="col-xs-10">
                                                    <input type="radio" style="vertical-align: middle" value="20" class="get_value_radio" id="cond" name="desconto"
                                                        <?php
                                                        if (!empty($desc) && ($desc == "S")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond"><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=S" ?>"> 20 dias dias c/ desc.</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="45" class="get_value_radio" id="cond2" name="desconto" 
                                                    <?php
                                                    if (!empty($desc) && ($desc == "N")) {
                                                        echo "checked";
                                                    }
                                                    ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=N" ?>"> 45 dias direto</a></label>
                                                </div>
                                            </form>
                                        </label></td>
                                </tr>
                                                    <?php
                                                }
                                            } elseif (($uf['UFFATURAMENTO'] == 'DF') ||
                                                    ($uf['UFFATURAMENTO'] == 'ES') ||
                                                    ($uf['UFFATURAMENTO'] == 'GO') ||
                                                    ($uf['UFFATURAMENTO'] == 'MG') ||
                                                    ($uf['UFFATURAMENTO'] == 'MS') ||
                                                    ($uf['UFFATURAMENTO'] == 'PR') ||
                                                    ($uf['UFFATURAMENTO'] == 'RJ') ||
                                                    ($uf['UFFATURAMENTO'] == 'RS') ||
                                                    ($uf['UFFATURAMENTO'] == 'SC') ||
                                                    ($uf['UFFATURAMENTO'] == 'SP')) {


                                                if ((($uf['UFFATURAMENTO'] == 'DF') ||
                                                        ($uf['UFFATURAMENTO'] == 'ES') ||
                                                        ($uf['UFFATURAMENTO'] == 'GO') ||
                                                        ($uf['UFFATURAMENTO'] == 'MG') ||
                                                        ($uf['UFFATURAMENTO'] == 'MS') ||
                                                        ($uf['UFFATURAMENTO'] == 'PR') ||
                                                        ($uf['UFFATURAMENTO'] == 'RJ') ||
                                                        ($uf['UFFATURAMENTO'] == 'RS') ||
                                                        ($uf['UFFATURAMENTO'] == 'SC') ||
                                                        ($uf['UFFATURAMENTO'] == 'SP')) && ($total > '400.0') && ($total < '900.0')) {
                                                    ?>

                                <tr>
                                    <td colspan="5" style="vertical-align:middle;"><label>Condições de Pagamento no Boleto</label></td>
                                    <td colspan="3"><label>
                                            <form>
                                                <div class="col-xs-10">
                                                    <input type="radio" style="vertical-align: middle" value="15" class="get_value_radio" id="cond" name="desconto"
                                                        <?php
                                                        if (!empty($desc) && ($desc == "S")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true"/>
                                                    <br/>
                                                    <label for="cond"><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=S" ?>"> 15 dias dias c/ desc.</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="30" class="get_value_radio" id="cond2" name="desconto" 
                                                    <?php
                                                    if (!empty($desc) && ($desc == "N")) {
                                                        echo "checked";
                                                    }
                                                    ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=N" ?>"> 30 dias direto</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="30-60" class="get_value_radio" id="cond3" name="desconto" 
                                                    <?php
                                                    if (!empty($desc) && ($desc == "NN")) {
                                                        echo "checked";
                                                    }
                                                    ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=NN" ?>"> 30/60 dias</a></label>
                                                </div>
                                            </form>
                                        </label></td>
                                </tr>
                                    <?php
                                } elseif ((($uf['UFFATURAMENTO'] == 'DF') ||
                                        ($uf['UFFATURAMENTO'] == 'ES') ||
                                        ($uf['UFFATURAMENTO'] == 'GO') ||
                                        ($uf['UFFATURAMENTO'] == 'MG') ||
                                        ($uf['UFFATURAMENTO'] == 'MS') ||
                                        ($uf['UFFATURAMENTO'] == 'PR') ||
                                        ($uf['UFFATURAMENTO'] == 'RJ') ||
                                        ($uf['UFFATURAMENTO'] == 'RS') ||
                                        ($uf['UFFATURAMENTO'] == 'SC') ||
                                        ($uf['UFFATURAMENTO'] == 'SP')) && ($total > '900.0')) {
                                    ?>

                                <tr>
                                    <td colspan="5" style="vertical-align:middle;"><label>Condições de Pagamento no Boleto</label></td>
                                    <td colspan="3"><label>
                                            <form>
                                                <div class="col-xs-10">
                                                    <input type="radio" style="vertical-align: middle" value="15" class="get_value_radio" id="cond" name="desconto"
                                                        <?php
                                                        if (!empty($desc) && ($desc == "S")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond"><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=S" ?>"> 15 dias dias c/ desc.</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="30" class="get_value_radio" id="cond2" name="desconto" 
                                                    <?php
                                                    if (!empty($desc) && ($desc == "N")) {
                                                        echo "checked";
                                                    }
                                                    ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=N" ?>"> 30 dias direto</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="30-60-90" class="get_value_radio" id="cond3" name="desconto" 
                                                    <?php
                                                    if (!empty($desc) && ($desc == "NN")) {
                                                        echo "checked";
                                                    }
                                                    ?>
                                                           disabled="true" />
                                                    <br/>
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=NN" ?>"> 30/60/90 dias</a></label>
                                                </div>
                                            </form>
                                        </label></td>
                                        </tr>
                                                            <?php
                                                        } else {
                                                            ?>
                                        <tr>
                                    <td colspan="5" style="vertical-align:middle;"><label>Condições de Pagamento no Boleto</label></td>
                                    <td colspan="3"><label>
                                            <form>
                                                <div class="col-xs-10">
                                                    <input type="radio" style="vertical-align: middle" value="15" class="get_value_radio" id="cond" name="desconto"
                                                        <?php
                                                        if (!empty($desc) && ($desc == "S")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <label for="cond"><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=S" ?>"> 15 dias c/ desc.</a></label>
                                                    <br/>
                                                    <input type="radio" style="vertical-align: middle" value="30" class="get_value_radio" id="cond2" name="desconto" 
                                                        <?php
                                                        if (!empty($desc) && ($desc == "N")) {
                                                            echo "checked";
                                                        }
                                                        ?>
                                                           disabled="true" />
                                                    <label for="cond2" ><a href="<?php echo "carrinho.php?acao=up&codigo=" . $teste2 . "&desconto=N" ?>"> 30 dias direto</a></label>
                                                </div>
                                            </form>
                                        </label></td>
                                </tr>
                                     <?php
                              }
                               } else {
                                 echo "ERRO! ENTRE EM CONTATO COM A SÓ PÉS.";
                               }
                                  ?>
                        <tr>
                            
                           <td colspan="5" style="vertical-align:middle;">
                               <label>Materiais de Divulgação</label>
                           </td>
                           <td colspan="3" style="text-align:left;">
                               <input type="checkbox" id="panf" class="get_value" style="margin-left: 20%" name="panf" value="Kit-Panfletos"/> <label for="panf"> Kit Panfletos</label><hr style="margin-bottom:0;margin-top:0;" />
                               <input type="checkbox" id="cat" class="get_value" style="margin-left: 20%" name="cat" value="Catálogo"/> <label  for="cat"> Catálogo</label>
                           </td>
                       </tr>
                    <?php 
               if(($uf['UFFATURAMENTO'] == 'AM') ||
                  ($uf['UFFATURAMENTO'] == 'AP') ||
                  ($uf['UFFATURAMENTO'] == 'RR') ||
                  ($uf['UFFATURAMENTO'] == 'AC') ||
                  ($uf['UFFATURAMENTO'] == 'RO')){
                        echo '<tr>
                                <td colspan="1"></td>
                                    <td colspan="5">
                                       <label>Você possui suframa, os descontos serão aplicados no pedido final enviado ao seu email.</label>
                                    </td>
                                    <td colspan="2"></td>
                       </tr>';
                    } else {
                        echo '';
                    }
            
                    
                if($produtos != null && ($total > 100)){
                        echo '<tr>
                                <td colspan="1"></td>
                                    <td colspan="5">
                                       <button type="button" class="btn btn-primar" id="button" name="inserir" value="inserir"  data-toggle="modal" data-target="#exampleModalLong"><span class="glyphicon glyphicon-ok"></span> Finalizar Pedido</button>
                                    </td>
                                    <td colspan="2"></td>
                       </tr>';
                    } else {
                        echo '<tr>
                                <td colspan="1"></td>
                                    <td colspan="4">
                                       <button type="button" class="btn btn-info" disabled>Abaixo de R$100,00</button>
                                    </td>
                                    <td colspan="2"></td>
                       </tr>';
//                        echo '<tr><td></td><td colspan="6" ><label ><font size="1">Valor mínimo de compra R$ 100,00</font></label></td><td colspan="1"></td></tr>';
                    }
              
                    
                    ?>
                </tbody>
                <?php 
                
                    }
                    
                ?>
                
           </form>
        </table>
            
        <form ajax="true">
            <div class="col-md-7"></div>
            <br/>
            <?php 
            
           $obj = json_encode($produtos);
           
           $obj2 = json_decode($obj);
      
           $i = 0;
           foreach($obj2 as $insert){
               
               if(empty($insert->ipi)){
                   $insert->ipi = '0.00';
                   
               }else{
                   $insert->ipi;
               }
               if((($insert->codigo)== 3387)||(($insert->codigo)== 3388)){
                 
                 $insert->ipi = '0.00';
              }else{
                 
                 $insert->ipi;
              }
               echo '<input type="hidden" class="get_value_codigo" value="' . $insert->codigo . '" id="codigo" />'
                       . '<input type="hidden" class="get_value_preco" value="' . $insert->preco . '" id="preco" />'
                       . '<input type="hidden" class="get_value_qtd" value="' . $insert->qtd . '" id="qtd" />'
                       . '<input type="hidden" class="get_value_grade" value="' . $insert->tamanho . '" id="tamanho" />';
               
                echo '<input type="hidden" class="get_value_ipi" value="' . $insert->ipi . '" id="ipi" />';
               $i++;
               
             
           }
           
           $_SESSION['contador'] = $i;
           
           if(isset($_SESSION['tot_cartao']) != null){
            $tot_cart = $_SESSION['tot_cartao'];
           }
           ?>
          
            <input type="hidden" value="<?php echo $cli; ?>" id="cliente"/>
            <input type="hidden" value="<?php echo $frete ?>" id="frete"/>
        </form>
             <!-- Modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                  <h5>Deseja enviar o pedido?</h5>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success" id="add" data-toggle="modal" data-target="#pagamento">Pagar com Cartão de Crédito</button>
                <button type="button" class="btn btn-prima" id="add2" data-toggle="modal" data-target="#myModal">Enviar</button>
              </div>
            </div>
          </div>
        </div>
             
         <form class="needs-validation" novalidate action="https://mgmttst.level4pay.com/payments" id="form_cartao" method="post">
             
            <input type="hidden" name="MerchantID" value="L4Z9II"/>
            <input type="hidden" name="merchantReferenceCode" id="merchantReferenceCode" value="">
            <input type="hidden" name="ProductName[]" id="ProductName[]" value="Pedido">
            <input type="hidden" name="FreightAmount" id="FreightAmount" value="0">
            <input type="hidden" name="TaxAmount" id="TaxAmount" value="0">
            <input type="hidden" name="ProductSku[]" value="1">
            <input type="hidden" name="ProductUnitPrice[]" id="ProductUnitPrice[]" value="<?php echo $tot_cart; ?>">
            <input type="hidden" name="ProductTax[]" value="0">
            <input type="hidden" name="ProductQuantity[]" id="ProductQuantity[]" value="1">
            <input type="hidden" name="transactionType" id="transactionType" value="AuthCapture">
            <input type="hidden" name="BillToCustomerID" id="BillToCustomerID" value="<?php echo $idCustomer; ?>">
            <input type="hidden" class="form-control" id="SuccessURL" name="SuccessURL" value="http://10.0.0.16/farma/success_credit_card.php">
            <input type="hidden" class="form-control" id="billTo_firstName" name="BillToFirstName" placeholder="" value="">
            <input type="hidden" class="form-control" id="billTo_lastName" name="BillToLastName" placeholder="" value="">
            <input type="hidden" class="form-control" id="billTo_email" name="BillToEmail" placeholder="" value="">
            <input type="hidden" class="form-control" id="billTo_phoneNumber" name="BILLTO_PHONENUMBER" placeholder="" value="">
            <input type="hidden" class="form-control" id="billTo_city" name="BillToCity" placeholder="" value="">
            <input type="hidden" class="form-control" id="billTo_street1" name="BillToStreet1" placeholder="" value="">
            <input type="hidden" class="form-control" id="billTo_street2" name="BillToStreet2" value="">
            <input type="hidden" class="form-control" id="billTo_streetNumber" name="BillToStreetNumber" placeholder="" value="">
            <input type="hidden" class="form-control" id="billTo_postalCode" name="BillToPostalCode" placeholder="" value="">
            <input type="hidden" class="form-control" id="billTo_state" name="BillToState" value="">
            <input type="hidden" class="form-control" id="billTo_country" name="BillToCountry" value="">
                
         </form>
              
             <script>
             $('#add').click(function () {
                   $('#exampleModalLong').modal('hide');
                 });
             $('#add2').click(function () {
                   $('#exampleModalLong').modal('hide');
                 });
             </script>
             <script>
                 
           $(document).ready(function() {
               
           
           $("#button").click(function(){
               $cliente = $("#cliente").val();
                $.ajax({
                method: "POST",
                url: "recupera_id_pedido.php",
                data: { finaliza: "finaliza",
                        cliente: $cliente },
                    success: function(data){
                        $("#merchantReferenceCode").val(data);
                    }
                 });                
           })           
               
            $("#add").click(function(e) {
                
             e.preventDefault();
                    
             var ckb = [];
             var radio = [];
             var codigo = [];
             var preco = [];
             var qtd = [];
             var ipi = [];
             var grade = [];
             var desconto = [];
             var st = [];
             
             $('.get_value').each(function(){
                 if($(this).is(":checked")){
                     ckb.push($(this).val());
                 }
             });
             
             $('.get_value_radio').each(function(){
                 if($(this).is(":checked")){
                     radio.push($(this).val());
                 }
             });
             
             $('.get_value_codigo').each(function(){
                     codigo.push($(this).val());
             });
             
             $('.get_value_preco').each(function(){
                     preco.push($(this).val());
             });
             $('.get_value_qtd').each(function(){
                     qtd.push($(this).val());
             });
             $('.get_value_ipi').each(function(){
                     ipi.push($(this).val());
             });
             $('.get_value_grade').each(function(){
                     grade.push($(this).val());
             });
             $('.get_value_desconto').each(function(){
                     desconto.push($(this).val());
             });
             $('.get_value_st').each(function(){
                     st.push($(this).val());
             });
             
             ckb = ckb.toString();
             
             radio = radio.toString();
             
             codigo = codigo.toString();

             preco = preco.toString();
             
             qtd = qtd.toString();
             
             ipi = ipi.toString();
             
             grade = grade.toString();
             
             desconto = desconto.toString();
             
             st = st.toString();
             
            $cliente = $("#cliente").val();
            $frete = $("#frete").val();
            $refCode = $("#merchantReferenceCode").val();
            $retorna_dados_cliente = "retorna_dados_cliente";
            
             $.ajax({
                method: "POST",
                url: "cadastra.php",
                data: {
                    ckb: ckb,
                    radio: radio,
                    cliente: $cliente,
                    frete: $frete,
                    desconto: desconto,
                    st: st,
                    codigo: codigo,
                    preco: preco,
                    qtd: qtd,
                    ipi: ipi,
                    grade: grade,
                    refCode: $refCode,
                    dados_cliente: $retorna_dados_cliente
                    },
                    success: function(data){
                       var json_obj = $.parseJSON(data);
                       
                       $("#billTo_firstName").val(json_obj.billTo_firstName);
                       $("#billTo_lastName").val(json_obj.billTo_lastName);
                       $("#billTo_email").val(json_obj.billTo_email);
                       $("#billTo_phoneNumber").val(json_obj.billTo_phoneNumber);
                       $("#billTo_city").val(json_obj.billTo_city);
                       $("#billTo_street1").val(json_obj.billTo_street1);
                       $("#billTo_street2").val(json_obj.billTo_street2);
                       $("#billTo_streetNumber").val(json_obj.billTo_streetNumber);
                       $("#billTo_postalCode").val(json_obj.billTo_postalCode);
                       $("#billTo_state").val(json_obj.billTo_state);
                       $("#billTo_country").val(json_obj.billTo_country);
                       
                       $("#myModal2").modal();
                       $("#form_cartao").submit();
                    }
                 });
                  return false;
              });
              
            $("#add2").click(function(e) {
                
             e.preventDefault();
                    
             var ckb = [];
             var radio = [];
             var codigo = [];
             var preco = [];
             var qtd = [];
             var ipi = [];
             var grade = [];
             var desconto = [];
             var st = [];
             
             $('.get_value').each(function(){
                 if($(this).is(":checked")){
                     ckb.push($(this).val());
                 }
             });
             
             $('.get_value_radio').each(function(){
                 if($(this).is(":checked")){
                     radio.push($(this).val());
                 }
             });
             
             $('.get_value_codigo').each(function(){
                     codigo.push($(this).val());
             });
             
             $('.get_value_preco').each(function(){
                     preco.push($(this).val());
             });
             $('.get_value_qtd').each(function(){
                     qtd.push($(this).val());
             });
             $('.get_value_ipi').each(function(){
                     ipi.push($(this).val());
             });
             $('.get_value_grade').each(function(){
                     grade.push($(this).val());
             });
             $('.get_value_desconto').each(function(){
                     desconto.push($(this).val());
             });
             $('.get_value_st').each(function(){
                     st.push($(this).val());
             });
             
             ckb = ckb.toString();
             
             radio = radio.toString();
             
             codigo = codigo.toString();

             preco = preco.toString();
             
             qtd = qtd.toString();
             
             ipi = ipi.toString();
             
             grade = grade.toString();
             
             desconto = desconto.toString();
             
             st = st.toString();
             
            $cliente = $("#cliente").val();
            $frete = $("#frete").val();
            
             $.ajax({
                method: "POST",
                url: "cadastra_gespal.php",
                data: {
                    ckb: ckb,
                    radio: radio,
                    cliente: $cliente,
                    frete: $frete,
                    desconto: desconto,
                    st: st,
                    codigo: codigo,
                    preco: preco,
                    qtd: qtd,
                    ipi: ipi,
                    grade: grade
                    },
                    success: function(data){
                         $("#myModal").modal();
                         setTimeout(function () {
                            $('#myModal').modal('hide');
                            location.reload();
                        }, 1000);
                    }
                 });
                  return false;
              });
              });            
             </script>
             
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
            <br/>
         <div class="alert alert-success" role="alert">
             <strong>
                 <p> Pedido Enviado</p>
             </strong>
         </div>
        </div>
      </div>
    </div>
  </div>
   <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
            <br/>
         <div class="alert alert-success" role="alert">
             <strong>
                 <p> Redirecionando...</p>
             </strong>
         </div>
        </div>
      </div>
    </div>
  </div>
             
 </div>
 </div>
</div>
</body>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>