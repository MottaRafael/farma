firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.
    document.getElementById("user_div").style.display = "block";
    document.getElementById("login_div").style.display = "none";

    var user = firebase.auth().currentUser;

    if(user != null){

      var email_id = user.email;
      var email_verified = user.emailVerified;
	  
    if(email_verified){
        document.getElementById("btn_verificar").style.display= "none";
      }else{
        document.getElementById("btn_verificar").style.display= "block";
      }
	  
      document.getElementById("user_para").innerHTML = "Bem vindo : " + email_id +
                                                        "<br/>Verificado : " + email_verified;
    }

  } else {

    document.getElementById("user_div").style.display = "none";
    document.getElementById("login_div").style.display = "block";

  }
});

function login(){
    
  var userEmail = document.getElementById("email_field").value;
  var userPass = document.getElementById("password_field").value;

  firebase.auth().signInWithEmailAndPassword(userEmail, userPass).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;

    window.alert("Error : " + errorMessage);


  });
}

function cadastrar(){

  var userEmail = document.getElementById("email_field").value;
  var userPass = document.getElementById("password_field").value;

  firebase.auth().createUserWithEmailAndPassword(userEmail, userPass).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;

    window.alert("Error : " + errorMessage);
    
  });
}

function verificar(){
  var user = firebase.auth().currentUser;

  user.sendEmailVerification().then(function(){
    window.alert("Verificação Enviada!");
  }).catch(function(error){
    window.alert("Erro: " + error.message);
  });
}

function logout(){
  firebase.auth().signOut();
}
