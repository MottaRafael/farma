<?php

 require ('../classes/conexao.php');

$codigo = $_POST['codigo']; 
 
$pdo = conecta();
$tes = $pdo ->prepare("select EMAIL_APP, NOME from CLIENTES where CODIGO=".$codigo);
$tes->execute();
$email = $tes->fetch(PDO::FETCH_ASSOC);

if(empty($email['NOME'])){
    $email2 = "CLIENTE NÃO ENCONTRADO";
}elseif (empty($email['EMAIL_APP']) && (!empty($email['NOME']))){
    $email2 = "CLIENTE NÃO POSSUI EMAIL CADASTRADO";
}else{
    $email2 = $email['EMAIL_APP'];
}
function gerar_senha($tamanho, $numeros){
  $nu = "0123456789";
    if ($numeros){
         return substr(str_shuffle($nu),0,$tamanho);
    }
}
    
?>
<html>
<head>
  <title>Cadastrar Cliente</title>
  <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
  <link rel="stylesheet" href="../cadastraUsuario/style.css" />
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#ajax_form').submit(function(){
			var dados = $( this ).serialize();

			$.ajax({
				type: "POST",
				url: "../enviar_email.php",
				data: dados,
				success: function( data )
				{
					alert( data );
				}
			});
			return false;
		});
	});
	</script>
</head>
<body>
    <form method="post" id="ajax_form">
    <div id="login_div" class="main-div">
    <h3>Cadastrar Cliente no App</h3>
    <input type="email" name="email" placeholder="E-mail" id="email_field" value="<?php echo $email2; ?>"/>
    <input type="password" placeholder="Senha" name="senha" value="<?php echo gerar_senha(9, true);?>" id="password_field" />
    <?php 
    if(empty($email['EMAIL_APP']) || (empty($email['NOME']))){
        echo '<a href="../primeiro.php"><button> Voltar </button></a>';
    }else{
        echo '<br/>
    <button type="submit" onclick="cadastrar()">Cadastrar</button>';        
    }
?>   
        </div>
    </form>
  <div id="user_div" class="loggedin-div">
    <h3>Bem vindo</h3>
    <p id="user_para">Logado!</p>
    <button onclick="logout()">Sair</button>
    <br/>
    <button onclick="verificar()">Enviar Verificação</button>
  </div>
  <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>
  <script>
     var config = {
    apiKey: "AIzaSyBfJX0iR8zFcIVteDhTAv-PwnBhEQkYpfY",
    authDomain: "so-pes.firebaseapp.com",
    databaseURL: "https://so-pes.firebaseio.com",
    projectId: "so-pes",
    storageBucket: "so-pes.appspot.com",
    messagingSenderId: "814567300328"
  };
  firebase.initializeApp(config);
  </script>
  <script src="index.js"></script>
</body>
</html>