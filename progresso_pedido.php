<?php
session_start();
$_SESSION['modal'] = true;

date_default_timezone_set('America/Sao_Paulo');
require_once ('classes/conexao.php');




$pdo = conecta();

if (isset($_GET['email'])) {
    $teste2 = $_GET['email'];
    $tes = $pdo->prepare("select CODIGO, NRO_LOJAS from CLIENTES where EMAIL_APP='" . $teste2 . "'");
    $tes->execute();
    $testando = $tes->fetch(PDO::FETCH_ASSOC);
    $idCustomer = $testando['CODIGO'];
} elseif (isset($_GET['codigo'])) {
    $idCustomer = $_GET['codigo'];
}

$sql = $pdo->prepare("select CLIENTES.CODIGO, PEDIDOS_PRE.PEDIDO, PEDIDOS_PRE.STATUS "
        . "from CLIENTES "
        . "INNER JOIN PEDIDOS_PRE ON (CLIENTES.CODIGO = PEDIDOS_PRE.COD_CLIENTE)"
        . "where CLIENTES.CODIGO='" . $idCustomer . "' order by PEDIDOS_PRE.PEDIDO desc ");
$sql->execute();
$status = $sql->fetch(PDO::FETCH_ASSOC);

?>
    <head>
        <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,  maximum-scale=1.0, minimum-scale=1.0">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <style>
            .bs-wizard {margin-top: 40px;}
            .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
            .bs-wizard  .bs-wizard-step {padding: 0; position: relative;}
            .bs-wizard  .bs-wizard-step + .bs-wizard-step {}
            .bs-wizard  .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
            .bs-wizard  .bs-wizard-step .bs-wizard-info {color: #808080; font-size: 14px;}
            .bs-wizard  .bs-wizard-step .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #00B4BD; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
            .bs-wizard  .bs-wizard-step .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #198c8c; border-radius: 50px; position: absolute; top: 8px; left: 8px; } 
            .bs-wizard .bs-wizard-step .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
            .bs-wizard  .bs-wizard-step .progress > .progress-bar {width:0px; box-shadow: none; background: #00B4BD;}
            .bs-wizard  .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
            .bs-wizard  .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
            .bs-wizard  .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
            .bs-wizard  .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
            .bs-wizard  .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
            .bs-wizard  .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
            .bs-wizard  .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
            .bs-wizard  .bs-wizard-step:last-child  > .progress {width: 50%;}
            .bs-wizard  .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
            
            
            
            
            .btn-primar {
            color: #fff;
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar:focus,
          .btn-primar.focus {
            color: #fff;
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar:hover {
            color: #fff;
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar:active,
          .btn-primar.active,
          .open > .dropdown-toggle.btn-primar {
            color: #fff;
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar:active:hover,
          .btn-primar.active:hover,
          .open > .dropdown-toggle.btn-primar:hover,
          .btn-primar:active:focus,
          .btn-primar.active:focus,
          .open > .dropdown-toggle.btn-primar:focus,
          .btn-primar:active.focus,
          .btn-primar.active.focus,
          .open > .dropdown-toggle.btn-primar.focus {
            color: #fff;
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar:active,
          .btn-primar.active,
          .open > .dropdown-toggle.btn-primar {
            background-image: none;
          }
          .btn-primar.disabled:hover,
          .btn-primar[disabled]:hover,
          fieldset[disabled] .btn-primar:hover,
          .btn-primar.disabled:focus,
          .btn-primar[disabled]:focus,
          fieldset[disabled] .btn-primar:focus,
          .btn-primar.disabled.focus,
          .btn-primar[disabled].focus,
          fieldset[disabled] .btn-primar.focus {
            background-color: #008B8B;
            border-color: #008B8B;
          }
          .btn-primar .badge {
            color: #008B8B;
            background-color: #fff;
          }
            
          
          
          
           .btn-prima {
            color: #fff;
            background-color: #FFFFFF;
            border-color: #FFFFFF;
          }
          .btn-prima:focus,
          .btn-prima.focus {
            color: #fff;
            background-color: #FFFFFF;
            border-color: #FFFFFF;
          }
          .btn-prima:hover {
            color: #fff;
            background-color: #FFFFFF;
            border-color: #FFFFFF;
          }
          .btn-prima:active,
          .btn-prima.active,
          .open > .dropdown-toggle.btn-prima {
            color: #fff;
            background-color: #FFFFFF;
            border-color: #FFFFFF;
          }
          .btn-prima:active:hover,
          .btn-prima.active:hover,
          .open > .dropdown-toggle.btn-prima:hover,
          .btn-prima:active:focus,
          .btn-prima.active:focus,
          .open > .dropdown-toggle.btn-prima:focus,
          .btn-prima:active.focus,
          .btn-prima.active.focus,
          .open > .dropdown-toggle.btn-prima.focus {
            color: #fff;
            background-color: #FFFFFF;
            border-color: #FFFFFF;
          }
          .btn-prima:active,
          .btn-prima.active,
          .open > .dropdown-toggle.btn-prima {
            background-image: none;
          }
          .btn-prima.disabled:hover,
          .btn-prima[disabled]:hover,
          fieldset[disabled] .btn-prima:hover,
          .btn-prima.disabled:focus,
          .btn-prima[disabled]:focus,
          fieldset[disabled] .btn-prima:focus,
          .btn-prima.disabled.focus,
          .btn-prima[disabled].focus,
          fieldset[disabled] .btn-prima.focus {
            background-color: #FFFFFF;
            border-color: #FFFFFF;
          }
          .btn-prima .badge {
            color: #FFFFFF;
            background-color: #fff;
          }
        </style>
    </head>
<body>
    <?php 
                    
            $sql_apaga = $pdo->prepare("select OUTRAS_RAZOES.cod_razao,CLIENTES.COD_GRUPO, clientes.matriz_filial as MATRIZ from clientes "
            . "inner join outras_razoes on (clientes.codigo = outras_razoes.cod_razao) "
            . "where OUTRAS_RAZOES.cod_cliente=" . $idCustomer);
            $sql_apaga->execute();
            $apagando = $sql_apaga->fetch(PDO::FETCH_ASSOC);

            if(!empty($apagando['COD_RAZAO'])){
                echo '<button class="btn btn-primar" id="botao" style="margin: 5% auto;margin-left: 35%">TROCAR LOJA</button>';
            }
            
        ?>

    <!--<div class="container">-->
        <!--<div class="row">-->
          <!--<div class="col-sm-10 col-md-12 side-content">-->
            <div class="row bs-wizard" style="border-bottom:0;">
                <?php 
                    $datahora=date('H:i');
                    if((($status['STATUS'] == 0) || ($status['STATUS']== 20) || ($status['STATUS']== 15) || ($status['STATUS']== 18) || ($status['STATUS'] == 7) || ($status['STATUS'] == 21) || ($status['STATUS']== 17) || ($status['STATUS']== 13) || ($status['STATUS']== 10) || ($status['STATUS']== 4) || ($status['STATUS']== 3) || ($status['STATUS']== 2) || ($status['STATUS']== 1) || ($status['STATUS']== 5) || ($status['STATUS']== 11)) || (($status['STATUS'] == 11)&&(($datahora) >= "17:30"))) {
                        if(($status['STATUS'] == 0)|| ($status['STATUS']== 20) || ($status['STATUS']== 18) || ($status['STATUS']== 17) || ($status['STATUS']== 13)|| ($status['STATUS']== 10) || ($status['STATUS']== 4) || ($status['STATUS']== 3) || ($status['STATUS']== 2) || ($status['STATUS']== 1)){
                            echo '<div class="col-xs-3 bs-wizard-step complete">
                            <div class="text-center bs-wizard-stepnum">1</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido Recebido</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step disabled">  
                            <div class="text-center bs-wizard-stepnum">2</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido Aprovado</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step disabled">  
                            <div class="text-center bs-wizard-stepnum">3</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Emissão da Nota Fiscal</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step disabled">  
                            <div class="text-center bs-wizard-stepnum">4</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido a Caminho</div>
                          </div>';
                        }else if($status['STATUS'] == 5){
                             echo '<div class="col-xs-3 bs-wizard-step complete">
                            <div class="text-center bs-wizard-stepnum">1</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido Recebido</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step complete">  
                            <div class="text-center bs-wizard-stepnum">2</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido Aprovado</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step disabled">  
                            <div class="text-center bs-wizard-stepnum">3</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Emissão da Nota Fiscal</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step disabled">  
                            <div class="text-center bs-wizard-stepnum">4</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido a Caminho</div>
                          </div>';
                        }else if($status['STATUS'] == 23){
                            echo '<div class="col-xs-3 bs-wizard-step complete">
                            <div class="text-center bs-wizard-stepnum">1</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido Recebido</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step complete">  
                            <div class="text-center bs-wizard-stepnum">2</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido Aprovado</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step complete">
                            <div class="text-center bs-wizard-stepnum">3</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Emissão da Nota Fiscal</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step complete">  
                            <div class="text-center bs-wizard-stepnum">4</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido a Caminho</div>
                          </div>';
                        }else if ($status['STATUS'] == 11){
                             echo '<div class="col-xs-3 bs-wizard-step complete">
                            <div class="text-center bs-wizard-stepnum">1</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido Recebido</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step complete">  
                            <div class="text-center bs-wizard-stepnum">2</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido Aprovado</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step complete">
                            <div class="text-center bs-wizard-stepnum">3</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Emissão da Nota Fiscal</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step disabled">  
                            <div class="text-center bs-wizard-stepnum">4</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido a Caminho</div>
                          </div>';
                        }else if(($status['STATUS'] == 7) || ($status['STATUS'] == 21)){
                             echo '<div class="col-xs-12">
                            <div class="col-xs-3 bs-wizard-step complete">
                            <div class="text-center bs-wizard-stepnum">1</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido Recebido</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step active" >  
                            <div class="text-center bs-wizard-stepnum">2</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center"><p style="color:red">Pedido Reprovado</p></div>
                          </div><br/>';
                          echo  '<div class="col-xs-6"><p  style="text-align: justify;color:red">Entre em contato pelo fone:<br/> 0800 600 1212.</p></div></div>';
                        }else if($status['STATUS'] == 15){
                             echo '<div class="col-xs-12">
                            <div class="col-xs-3 bs-wizard-step complete">
                            <div class="text-center bs-wizard-stepnum">1</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido Recebido</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step complete">  
                            <div class="text-center bs-wizard-stepnum">2</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Pedido Aprovado</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step complete">
                            <div class="text-center bs-wizard-stepnum">3</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Emissão da Nota Fiscal</div>
                          </div>
                          
                          <div class="col-xs-3 bs-wizard-step active" >  
                            <div class="text-center bs-wizard-stepnum">4</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center"><p style="color:red">Pedido Extraviado</p></div>
                            
                            </div>
                            <br/>';
                          echo  '<div class="col-xs-12"><p  style="text-align: justify;color:red">Entre em contato pelo fone: 0800 600 1212.</p></div>';
                        }
                    } else {
                    echo  '<br/><div class="col-xs-10"><p  style="text-align: justify;opacity: 0.9;">Nenhum pedido recebido.</p></div>';
                    }
                    
//                   echo  $status['STATUS'];
                    
                ?>
            </div>
           <!--</div>-->
        <!--</div>-->
    <!--</div>-->
    <form  method="GET" id="exemplo_sug" action="<?php $_SERVER['PHP_SELF']; ?>">
            <?php
            $sql_novo = $pdo->prepare("select OUTRAS_RAZOES.cod_razao, CLIENTES.COD_GRUPO,clientes.matriz_filial as MATRIZ from clientes "
                    . "inner join outras_razoes on (clientes.codigo = outras_razoes.cod_razao) "
                    . "where OUTRAS_RAZOES.cod_cliente=" . $idCustomer);
            $sql_novo->execute();
            $testando2 = $sql_novo->fetch(PDO::FETCH_ASSOC);
            $matriz_filial = $testando2['MATRIZ'];
            $matriz = trim($matriz_filial);
            $et[] = $testando2['COD_RAZAO'];
            $et2 = count($et);
            if (($et2 >= 1) && (empty($_SESSION['razao'])) && (($matriz == "M") || ($matriz == "F")) && (($testando2['COD_GRUPO'] == 2) || ($testando2['COD_GRUPO'] == 12) || ($testando2['COD_GRUPO'] == 62))) {
                ?>
                <script>
                    $("#botao").click(function () {
                        $('#exemplomodal_sug').modal('show');
                    });
                </script>
                <?php
                $novo = $pdo->prepare("select OUTRAS_RAZOES.cod_razao,CLIENTES.cgc,CLIENTES.ENDERECOFATURAMENTO, CLIENTES.NOME "
                        . "from clientes "
                        . "inner join outras_razoes on (clientes.codigo = outras_razoes.cod_razao) "
                        . "where OUTRAS_RAZOES.cod_cliente=" . $idCustomer);
                $novo->execute();
                $novo2 = $pdo->prepare("select NOME, CODIGO, CGC, ENDERECOFATURAMENTO FROM CLIENTES WHERE CODIGO=" . $idCustomer);
                $novo2->execute();
                $novo_sql2 = $novo2->fetch(PDO::FETCH_ASSOC);
                echo '<div class="modal fade" id="exemplomodal_sug"  role="dialog">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header" style="text-align: center;">
                                  <h4 class="modal-title"><label style="margin:3%">Escolha a loja para visualizar o status</label></h4>
                                </div>
                                <div class="modal-body"><br/>
                                <label for="nome"><font size="1">RAZÃO: ' . $novo_sql2['NOME'] . '</font></label><br/>'
                . '<input type="radio" style="float: right;" value="' . $idCustomer . '" id="nome" name="codigo" checked/>'
                . '<label for="nome"><font size="1"> CNPJ: ' . $novo_sql2['CGC'] . '</font></label><br/>'
                . '<label for="nome"><font size="1">' . $novo_sql2['ENDERECOFATURAMENTO'] . '</font></label><br/>'
                . '<hr/>';
                $y = 0;
                while ($novo_sql = $novo->fetch(PDO::FETCH_ASSOC)) {
                    $y++;
                    echo
                    '<label for="nome' . $y . '"><font size="1">RAZÃO: ' . $novo_sql['NOME'] . '</font></label><br/>'
                    . '<input type="radio"  style="float: right;" value="' . $novo_sql['COD_RAZAO'] . '" id="nome' . $y . '" name="codigo"/>'
                    . '<label for="nome' . $y . '"><font size="1"> CNPJ: ' . $novo_sql['CGC'] . '</font></label><br/>'
                    . '<label for="nome' . $y . '"><font size="1">' . $novo_sql['ENDERECOFATURAMENTO'] . '</font></label>'
                            . '<hr/>';
                }
                echo '<br/>
                        <div class="modal-footer">
                        <input type="submit" id="addexemplo" style="width:30%" class="btn btn-primar" value="OK">
                    </div>
                  </div>
                </div>
              </div>';
            }
            
            ?>
                
        </form>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>