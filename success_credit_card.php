<?php
session_start();
require_once ('classes/conexao.php');
date_default_timezone_set('America/Sao_Paulo');
$datahora = date('d.m.Y H:i');
$data = date('d.m.Y');
$pdo = conecta_mysql();
$pdo_gespal = conecta();

$id = $_POST['billTo_customerID'];

$select_gespal = ("SELECT EMAIL_APP FROM CLIENTES WHERE CODIGO=".$id);
$select_gespal = $pdo_gespal->prepare($select_gespal);
$select_gespal->execute();
$linha_gespal = $select_gespal->fetch(PDO::FETCH_ASSOC);
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style>
    .container-fluid {
    display: flex;
    align-items: center;
    justify-content: center; 
    margin-top: 20%;
}
</style>
<body style="background-color: #00B4BD; margin-left: 0px;" id="principal">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="alert alert-success alert-dismissible fade in text-center" role="alert">
                <strong>Feito!</strong> Pagamento concluído.
                <a href="index.php?email=<?php echo $linha_gespal['EMAIL_APP']; ?>" >
                    <p style="font-size:12"><span class="glyphicon glyphicon-shopping-cart" ></span> Continuar Comprando</p>
                </a>
            </div>
        </div>
        
  </div>
</div>
    
<?php

$zero = 0;
$ponto = '.';
$dois = 2;
$n = 'N';
$p = 'P';
$s = 'S';

if(($_POST['cardInstallments']) == "1"){
    
    $cond1 = "30";
    $cond2 = "0";
    $cond3 = "0";
    $cond4 = "0";
    $cond5 = "0";
    $cond6 = "0";
    $cond7 = "0";
    $cond8 = "0";
    $cond9 = "0";
    $cond10 = "0";
    $cond11 = "0";
    $cond12 = "0";
    
}else if(($_POST['cardInstallments']) == "2"){
    
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "0";
    $cond4 = "0";
    $cond5 = "0";
    $cond6 = "0";
    $cond7 = "0";
    $cond8 = "0";
    $cond9 = "0";
    $cond10 = "0";
    $cond11 = "0";
    $cond12 = "0";  
    
}else if(($_POST['cardInstallments']) == "3"){
    
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "90";
    $cond4 = "0";
    $cond5 = "0";
    $cond6 = "0";
    $cond7 = "0";
    $cond8 = "0";
    $cond9 = "0";
    $cond10 = "0";
    $cond11 = "0";
    $cond12 = "0";
    
}else if(($_POST['cardInstallments']) == "4"){
    
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "90";
    $cond4 = "120";
    $cond5 = "0";
    $cond6 = "0";
    $cond7 = "0";
    $cond8 = "0";
    $cond9 = "0";
    $cond10 = "0";
    $cond11 = "0";
    $cond12 = "0";
    
}else if(($_POST['cardInstallments']) == "5"){
    
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "90";
    $cond4 = "120";
    $cond5 = "150";
    $cond6 = "0";
    $cond7 = "0";
    $cond8 = "0";
    $cond9 = "0";
    $cond10 = "0";
    $cond11 = "0";
    $cond12 = "0";
    
}else if(($_POST['cardInstallments']) == "6"){
    
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "90";
    $cond4 = "120";
    $cond5 = "150";
    $cond6 = "180";
    $cond7 = "0";
    $cond8 = "0";
    $cond9 = "0";
    $cond10 = "0";
    $cond11 = "0";
    $cond12 = "0";
    
}else if(($_POST['cardInstallments']) == "7"){
    
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "90";
    $cond4 = "120";
    $cond5 = "150";
    $cond6 = "180";
    $cond7 = "210";
    $cond8 = "0";
    $cond9 = "0";
    $cond10 = "0";
    $cond11 = "0";
    $cond12 = "0";
    
}else if(($_POST['cardInstallments']) == "8"){
    
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "90";
    $cond4 = "120";
    $cond5 = "150";
    $cond6 = "180";
    $cond7 = "210";
    $cond8 = "240";
    $cond9 = "0";
    $cond10 = "0";
    $cond11 = "0";
    $cond12 = "0";
    
}else if(($_POST['cardInstallments']) == "9"){
    
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "90";
    $cond4 = "120";
    $cond5 = "150";
    $cond6 = "180";
    $cond7 = "210";
    $cond8 = "240";
    $cond9 = "270";
    $cond10 = "0";
    $cond11 = "0";
    $cond12 = "0";
    
}else if(($_POST['cardInstallments']) == "10"){
    
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "90";
    $cond4 = "120";
    $cond5 = "150";
    $cond6 = "180";
    $cond7 = "210";
    $cond8 = "240";
    $cond9 = "270";
    $cond10 = "300";
    $cond11 = "0";
    $cond12 = "0";
    
}else if(($_POST['cardInstallments']) == "11"){
    
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "90";
    $cond4 = "120";
    $cond5 = "150";
    $cond6 = "180";
    $cond7 = "210";
    $cond8 = "240";
    $cond9 = "270";
    $cond10 = "300";
    $cond11 = "330";
    $cond12 = "0";
    
}else if(($_POST['cardInstallments']) == "12"){
    
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "90";
    $cond4 = "120";
    $cond5 = "150";
    $cond6 = "180";
    $cond7 = "210";
    $cond8 = "240";
    $cond9 = "270";
    $cond10 = "300";
    $cond11 = "330";
    $cond12 = "360";
    
}else{
    
    $cond1 = "30";
    $cond2 = "0";
    $cond3 = "0";
    $cond4 = "0";
    $cond5 = "0";
    $cond6 = "0";
    $cond7 = "0";
    $cond8 = "0";
    $cond9 = "0";
    $cond10 = "0";
    $cond11 = "0";
    $cond12 = "0";
    
}


//var_dump($_POST['cardInstallments']);
//var_dump($cond1);


if(isset($_POST['transactionDecision']) == "ACCEPT"){
    $id = $_POST['billTo_customerID'];
    $refCode = $_POST['merchantReferenceNumber'];
    if(isset($_POST['transactionDecision']) == "ACCEPT"){
        $status = "S";
        $update = ("update app_sopes_pedidos set status=:status where id=". $refCode);
        $update_status = $pdo->prepare($update);
        $update_status->bindParam(':status', $status, PDO::PARAM_STR);
        $update_status->execute();
    }else{
        $status = "N";
    }
   
    $sql1 = ("select id from app_sopes_clientes where id = ".$id);
    $stmt2 = $pdo->prepare($sql1);
    $stmt2->execute();
    $consulta = $stmt2->fetch(PDO::FETCH_ASSOC);
    

    if($stmt2->rowCount() > 0){
        
        $sql3 = ("update app_sopes_clientes set billTo_firstName=:billTo_firstName, billTo_lastName=:billTo_lastName, billTo_email=:billTo_email, "
                . "billTo_phoneNumber=:billTo_phoneNumber, billTo_city=:billTo_city, billTo_street1=:billTo_street1, billTo_street2=:billTo_street2, "
                . "billTo_streetNumber=:billTo_streetNumber,billTo_postalCode=:billTo_postalCode, billTo_state=:billTo_state, billTo_country=:billTo_country where id = :id");
       $stmt3 = $pdo->prepare($sql3);
       $stmt3->bindParam(':id', $id, PDO::PARAM_INT);
       $stmt3->bindParam(':billTo_firstName', $_POST['billTo_firstName'], PDO::PARAM_STR);
       $stmt3->bindParam(':billTo_lastName',  $_POST['billTo_lastName'], PDO::PARAM_STR);
       $stmt3->bindParam(':billTo_email',  $_POST['billTo_email'], PDO::PARAM_STR);
       $stmt3->bindParam(':billTo_phoneNumber', $_POST['billTo_phoneNumber'], PDO::PARAM_STR);
       $stmt3->bindParam(':billTo_city', $_POST['billTo_city'], PDO::PARAM_STR);
       $stmt3->bindParam(':billTo_street1', $_POST['billTo_street1'], PDO::PARAM_STR);
       $stmt3->bindParam(':billTo_street2', $_POST['billTo_street2'], PDO::PARAM_STR);
       $stmt3->bindParam(':billTo_streetNumber', $_POST['billTo_streetNumber'], PDO::PARAM_STR);
       $stmt3->bindParam(':billTo_postalCode', $_POST['billTo_postalCode'], PDO::PARAM_STR);
       $stmt3->bindParam(':billTo_state', $_POST['billTo_state'], PDO::PARAM_STR);
       $stmt3->bindParam(':billTo_country', $_POST['billTo_country'], PDO::PARAM_STR);
       $stmt3->execute();
       
    }else{
        
    $sql = ("insert into app_sopes_clientes (id, billTo_firstName, billTo_lastName, billTo_email, billTo_phoneNumber, billTo_city, billTo_street1, billTo_street2,"
            . " billTo_streetNumber,billTo_postalCode, billTo_state, billTo_country) "
            . "values (:id, :billTo_firstName, :billTo_lastName, :billTo_email, :billTo_phoneNumber, :billTo_city, :billTo_street1, :billTo_street2, "
            . ":billTo_streetNumber, :billTo_postalCode, :billTo_state, :billTo_country)");
       $stmt = $pdo->prepare($sql);
       $stmt->bindParam(':id', $id, PDO::PARAM_INT);
       $stmt->bindParam(':billTo_firstName', $_POST['billTo_firstName'], PDO::PARAM_STR);
       $stmt->bindParam(':billTo_lastName',  $_POST['billTo_lastName'], PDO::PARAM_STR);
       $stmt->bindParam(':billTo_email',  $_POST['billTo_email'], PDO::PARAM_STR);
       $stmt->bindParam(':billTo_phoneNumber', $_POST['billTo_phoneNumber'], PDO::PARAM_STR);
       $stmt->bindParam(':billTo_city', $_POST['billTo_city'], PDO::PARAM_STR);
       $stmt->bindParam(':billTo_street1', $_POST['billTo_street1'], PDO::PARAM_STR);
       $stmt->bindParam(':billTo_street2', $_POST['billTo_street2'], PDO::PARAM_STR);
       $stmt->bindParam(':billTo_streetNumber', $_POST['billTo_streetNumber'], PDO::PARAM_STR);
       $stmt->bindParam(':billTo_postalCode', $_POST['billTo_postalCode'], PDO::PARAM_STR);
       $stmt->bindParam(':billTo_state', $_POST['billTo_state'], PDO::PARAM_STR);
       $stmt->bindParam(':billTo_country', $_POST['billTo_country'], PDO::PARAM_STR);
       $stmt->execute();
       
    }
    
    
    $sql_gespal = $pdo_gespal->prepare('SELECT GEN_ID(gen_pedidos_pre_id, 1) as PED FROM RDB$DATABASE');
    $sql_gespal->execute();
    $pedido_gespal = $sql_gespal->fetch(PDO::FETCH_ASSOC);

    $sel_gespal = $pdo_gespal->prepare("SELECT REPRESENTANTE, COD_VENDEDOR, COD_CARTEIRA FROM CLIENTES WHERE CODIGO=". $id);
    $sel_gespal ->execute();
    $dados_gespal = $sel_gespal->fetch(PDO::FETCH_ASSOC);

    $select2_gespal = $pdo_gespal->prepare("SELECT COD_USUARIO, NOME FROM USUARIOS WHERE COD_CARTEIRA=".$dados_gespal['COD_CARTEIRA']);
    $select2_gespal ->execute();
    $dados2_gespal = $select2_gespal->fetch(PDO::FETCH_ASSOC);

    $select3_gespal = $pdo_gespal->prepare("SELECT COD_USUARIO, NOME FROM USUARIOS WHERE COD_USUARIO=956");
    $select3_gespal ->execute();
    $dados3_gespal = $select3_gespal->fetch(PDO::FETCH_ASSOC);

    $operador_gespal = $dados3_gespal['NOME'];
    
    $select_itens_mysql2 = ("select valor_frete, ckb from app_sopes_pedidos where id=". $refCode);
    $gespal_sel2 = $pdo->prepare($select_itens_mysql2);
    $gespal_sel2->execute();
    $consulta_itens_mysql2 = $gespal_sel2->fetch(PDO::FETCH_ASSOC);
            
    $sql2_gespal = ("insert into pedidos_pre (PEDIDO, COD_CLIENTE, COD_REPRES, COD_VEND, COD_SUPERVISOR, EMISSAO, ENTREGA, "
 . "OC, OPERADOR, PED_REPRES, PED_REQ, COND1, COND2, COND3, COND4, COND5, COND6, COND7, COND8, COND9, COND10, COND11, COND12, IMPRESSO, LIBERADO_IMPRESSAO, CANCELADO, GRAVADO_NAPHTA, "
 . "LIBERADO_CADASTRO, STATUS, VISTO, COD_REQUISICAO, COD_AMOSTRA, CARTEIRA, OBS, OBS_FINANCEIRO, OBS_COMERCIAL, EMPRESA, DE_ONDE, PERSONALIZADO, VALOR_FRETE) "
 . "values (:PEDIDO, :COD_CLIENTE, :COD_REPRES, :COD_VEND, :COD_SUPERVISOR, :EMISSAO, :ENTREGA, :OC, :OPERADOR, :PED_REPRES, :PED_REQ,"
         . " :COND1, :COND2, :COND3, :COND4, :COND5, :COND6,:COND7, :COND8, :COND9, :COND10, :COND11, :COND12, :IMPRESSO, :LIBERADO_IMPRESSAO, :CANCELADO, :GRAVADO_NAPHTA, :LIBERADO_CADASTRO,"
         . " :STATUS, :VISTO, :COD_REQUISICAO, :COD_AMOSTRA, :CARTEIRA, :OBS, :OBS_FINANCEIRO, :OBS_COMERCIAL, :EMPRESA, :DE_ONDE, :PERSONALIZADO, :VALOR_FRETE)");
        $stmt_gespal = $pdo_gespal->prepare($sql2_gespal);
        $stmt_gespal->bindParam(':PEDIDO', $pedido_gespal['PED'], PDO::PARAM_INT);
        $stmt_gespal->bindParam(':COD_CLIENTE', $id, PDO::PARAM_INT);
        $stmt_gespal->bindParam(':COD_REPRES', $dados_gespal['REPRESENTANTE'], PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COD_VEND', $zero, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COD_SUPERVISOR', $zero, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':EMISSAO', $datahora, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':ENTREGA', $data, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':OC', $refCode, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':OPERADOR', $operador_gespal, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':PED_REPRES', $n, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':PED_REQ', $p, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND1', $cond1, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND2', $cond2, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND3', $cond3, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND4', $cond4, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND5', $cond5, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND6', $cond6, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND7', $cond7, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND8', $cond8, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND9', $cond9, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND10', $cond10, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND11', $cond11, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COND12', $cond12, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':IMPRESSO', $n, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':LIBERADO_IMPRESSAO', $n, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':CANCELADO', $n, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':GRAVADO_NAPHTA', $n, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':LIBERADO_CADASTRO', $n, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':STATUS', $dois, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':VISTO', $s, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COD_REQUISICAO', $zero, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':COD_AMOSTRA', $zero, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':CARTEIRA', $dados_gespal['COD_CARTEIRA'], PDO::PARAM_STR);
        $stmt_gespal->bindParam(':OBS',  $consulta_itens_mysql2['ckb'], PDO::PARAM_STR);
        $stmt_gespal->bindParam(':OBS_FINANCEIRO', $ponto, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':OBS_COMERCIAL', $ponto, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':EMPRESA', $zero, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':DE_ONDE', $p, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':PERSONALIZADO', $n, PDO::PARAM_STR);
        $stmt_gespal->bindParam(':VALOR_FRETE', $consulta_itens_mysql2['valor_frete'], PDO::PARAM_STR);
        $stmt_gespal->execute();

    $select_itens_mysql = ("select * from app_sopes_pedidos_item where id_pedido=". $refCode);
    $gespal_sel = $pdo->prepare($select_itens_mysql);
    $gespal_sel->execute();
    
    while($consulta_itens_mysql = $gespal_sel->fetch(PDO::FETCH_ASSOC)){
    
    $sql3_gespal = ("insert into pedidos_item_pre (PEDIDO,COD_PRODUTO, GRADE, QTD, UNITARIO,IPI, DESCONTO, PERSONALIZADO, APLICOU_ACRESCIMO,VALOR_ICMS_SUBST_TRIB) "
        . "values (:PEDIDO, :COD_PRODUTO, :GRADE, :QTD, :UNITARIO, :IPI, :DESCONTO, :PERSONALIZADO, :APLICOU_ACRESCIMO, :VALOR_ICMS_SUBST_TRIB)");
       $stmt2_gespal = $pdo_gespal->prepare($sql3_gespal);
       $stmt2_gespal->bindParam(':PEDIDO', $pedido_gespal['PED'], PDO::PARAM_INT);
       $stmt2_gespal->bindParam(':COD_PRODUTO', $consulta_itens_mysql['cod_produto'], PDO::PARAM_STR);
       $stmt2_gespal->bindParam(':GRADE',  $consulta_itens_mysql['grade'], PDO::PARAM_STR);
       $stmt2_gespal->bindParam(':QTD',  $consulta_itens_mysql['qtd'], PDO::PARAM_STR);
       $stmt2_gespal->bindParam(':UNITARIO', $consulta_itens_mysql['unitario'], PDO::PARAM_STR);
       $stmt2_gespal->bindParam(':IPI', $consulta_itens_mysql['ipi'], PDO::PARAM_STR);
       $stmt2_gespal->bindParam(':DESCONTO', $consulta_itens_mysql['desconto'], PDO::PARAM_STR);
       $stmt2_gespal->bindParam(':PERSONALIZADO', $n, PDO::PARAM_STR);
       $stmt2_gespal->bindParam(':APLICOU_ACRESCIMO', $n, PDO::PARAM_STR);
       $stmt2_gespal->bindParam(':VALOR_ICMS_SUBST_TRIB', $consulta_itens_mysql['valor_icms_subst_trib'], PDO::PARAM_STR);
       $stmt2_gespal->execute();
       
    }
    
    session_destroy();
    
}

?>
</body>
