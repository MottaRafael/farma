<?php
require_once ('classes/conexao.php');


function ValorSubst($cliente, $produto, $unit, $qtd, $frete, $desconto) {
    $pdo = conecta();
    $teste = $pdo->prepare("select UFFATURAMENTO,INSCRICAOESTADUAL,coalesce(SIMPLES,'N') as SIMPLES,coalesce(MEI,'N') as MEI,CGC,CPF from CLIENTES where CODIGO=" . $cliente);
    $teste->execute();
    $proximo = $teste->fetch(PDO::FETCH_ASSOC);

    $teste2 = $pdo->prepare("select mva_ajustada.aliquota_interna, mva_ajustada.aliquota_externa, mva_ajustada.mva_ajustada, "
            . "coalesce(mva_ajustada.pct_mva_ajustada_simples,0) as pct_mva_ajustada_simples, "
            . "produtos.cod_natureza_dentro_estado, produtos.cod_natureza_fora_estado, produtos.seq_dentro_estado, "
            . "produtos.seq_fora_estado, coalesce(mva_ajustada.pct_fundo_combate_pobreza,0) as pct_fundo_combate_pobreza, "
            . "coalesce(produtos.pct_ipi,0) as pct_ipi "
            . "from produtos "
            . "inner join classificacoes_fiscais on (produtos.classificacao_fiscal = classificacoes_fiscais.descricao) "
            . "inner join mva_ajustada on (classificacoes_fiscais.cod_classificacao = mva_ajustada.cod_classificacao_fiscal) "
            . "inner join tabela_icms on (mva_ajustada.uf = tabela_icms.estado) "
            . "where produtos.codigo=" . $produto . " and mva_ajustada.uf='" . $proximo['UFFATURAMENTO'] . "' and produtos.tem_substituicao='S' and tabela_icms.subst_tributaria='S'");
    $teste2->execute();
    $proximo2 = $teste2->fetch(PDO::FETCH_ASSOC);
    if (!empty($proximo2)) {
        if ((trim($proximo['SIMPLES'] = "S")) && ($proximo2['PCT_MVA_AJUSTADA_SIMPLES'] > 0)) {
            $mvaAjustada = $proximo2['PCT_MVA_AJUSTADA_SIMPLES'];
            
        } else {
            $mvaAjustada = $proximo2['MVA_AJUSTADA'];
        }

        $aliquota_interna = $proximo2['ALIQUOTA_INTERNA'];
        $aliquota_externa = $proximo2['ALIQUOTA_EXTERNA'];

        $cod_natureza_dentro_estado = $proximo2['COD_NATUREZA_DENTRO_ESTADO'];
        $cod_natureza_fora_estado = $proximo2['COD_NATUREZA_FORA_ESTADO'];

        if (trim($proximo['UFFATURAMENTO'] == "RS")) {
            $teste3 = $pdo->prepare("select tem_substituicao_tributaria from naturezas_operacao where cod_natureza=" . $cod_natureza_dentro_estado . " and seq=" . $proximo2['SEQ_DENTRO_ESTADO']);
            $teste3->execute();
            $proximo3 = $teste3->fetch(PDO::FETCH_ASSOC);

            if (trim($proximo3['TEM_SUBSTITUICAO_TRIBUTARIA']) == "N") {
                $aliquota_externa = 0;
                $aliquota_interna = 0;
                $mvaAjustada = 0;
            }
        } else {
            $teste4 = $pdo->prepare("select tem_substituicao_tributaria from naturezas_operacao where cod_natureza=" . $cod_natureza_fora_estado . " and seq=" . $proximo2['SEQ_FORA_ESTADO']);
            $teste4->execute();
            $proximo4 = $teste4->fetch(PDO::FETCH_ASSOC);

            if (trim($proximo4['TEM_SUBSTITUICAO_TRIBUTARIA']) == "N") {
                $aliquota_externa = 0;
                $aliquota_interna = 0;
                $mvaAjustada = 0;
            }
        }

        $teste4 = $pdo->prepare("select pct_base_icms from produtos_base_icms_uf where uf='" . $proximo['UFFATURAMENTO'] . "' and cod_produto=" . $produto);
        $teste4->execute();
        $proximo4 = $teste4->fetch(PDO::FETCH_ASSOC);

        if (!empty($proximo4)) {
            $percBaseICMS = $proximo4['PCT_BASE_ICMS'];
        } else {
            $percBaseICMS = 100;
        }

        $valorProdutos = $unit * $qtd;
        $desconto = ($valorProdutos * $desconto) / 100;
        $ipi = ($valorProdutos - $desconto) * ($proximo2['PCT_IPI'] / 100);
        $valorProdutos2 = $valorProdutos + $ipi + $frete - $desconto;
        $base = ($valorProdutos2 * ($mvaAjustada / 100)) + $valorProdutos2;
        $ICMSInterno = round(($base * ($aliquota_interna / 100)),2);
        $aliquota_externa = round(($aliquota_externa * ($percBaseICMS / 100)),2);
        $ICMSProprio = (($valorProdutos2 - $ipi) * ($aliquota_externa / 100));
        $FCP = $base * ($proximo2['PCT_FUNDO_COMBATE_POBREZA'] / 100);
        
        $valorSubstTrib = $ICMSInterno - $ICMSProprio + $FCP;
        
        return $valorSubstTrib;
    } else {
        return $valorSubstTrib = '';
    }
}


function historicoPedidos($idCustomer){
    $pdo = conecta();
    try {
        $consultar = $pdo->prepare("SELECT PEDIDOS_PRE.PEDIDO, PEDIDOS_PRE.EMISSAO, NOTAS.NRO_NOTA "
                . "FROM PEDIDOS_PRE "
                . "INNER JOIN PEDIDOS ON (PEDIDOS_PRE.PEDIDO = PEDIDOS.NRO_INTERNO) "
                . "INNER JOIN NOTAS ON (NOTAS.PEDIDO = PEDIDOS.PEDIDO) "
                . "WHERE PEDIDOS_PRE.COD_CLIENTE=:idCustomer AND PEDIDOS_PRE.PED_REQ = 'P' "
                . "order by PEDIDOS_PRE.PEDIDO DESC ");
        $consultar->bindValue(':idCustomer', $idCustomer, PDO::PARAM_INT);
        $consultar->execute();
        echo "<div id=\"teste\" style=\"overflow: scroll;border:1px\"><table id=\"tbVendas\" rules=\"rows\">
                                <thead>
                                    <tr>
                                        <th cellspacing=\"2\"></th>
                                        <th><p style=\"color: #808080; height: 10\">Nota<p></th>
                                        <th><p style=\"color: #808080; height: 10\">Data<p></th>
                                        <th><p style=\"color: #808080; height: 10\">Ações<p></th>
                                    </tr>
                                </thead>
                                <tbody>";
        $i = 0;
        $x = 0;
        while ($resultado = $consultar->fetch(PDO::FETCH_ASSOC)){
            $i++;
            $x++;
             echo "<tr class=\"linhaVenda\"><td></td>";
             echo "<td>".$resultado['NRO_NOTA']."</td>";
             echo "<td>".date_format(new DateTime($resultado['EMISSAO']), "d/m/Y")."</td>";
             echo '<td>'
             . '<form method="post" id="form_rastreio'.$i.'">
                 <br/>
                    <input type="hidden" name="rastrear" id="rastrear'.$i.'" value="rastrear"/>
                    <input type="hidden" name="nota" id="nota'.$i.'" value="'.$resultado['NRO_NOTA'].'"/>
                    <button type="button" name="submit" id="submit" onclick="rastrearPedido'.$i.'()" class="btn btn-primar" ><i class="fa fa-truck"></i></button>
                    <input type="hidden" name="cliente" id="cliente'.$i.'" value="'.$idCustomer.'"/>
                </form>
                <form ajax="true">
                <button type="button" class="btn btn-primar" data-toggle="modal" data-target="#Modal_qtd'.$i.'"><i class="fa fa-envelope-square"></i></button>
                    <div class="modal fade" id="Modal_qtd'.$i.'"  role="dialog">
                                            <div class="modal-dialog">
                                              <div class="modal-content">
                                                <div class="modal-header" style="text-align: center;">
                                                  <button type="button" class="close" data-dismiss="modal" style="margin:3%">&times;</button>
                                                  <h4 class="modal-title"><label style="margin:3%">Enviar nota por email</label></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="email" name="email" value="" placeholder="Inserir E-mail"/>
                                                    <input type="hidden" name="nota" id="nota" value="'.$resultado['NRO_NOTA'].'" placeholder="."/>
                                                        <br/><br/>
                                                    <input type="radio" name="acao" id="pdf" value="pdf" checked> <label for="pdf">PDF</label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input type="radio" name="acao" id="xml" value="xml"> <label for="xml">XML</label>
                                                    <br/>
                                               </div> 
                                             <div class="modal-footer">
                                        <button type="submit" id="add'.$i.'" style="width:30%" class="btn btn-primar" data-toggle="modal" data-target="#myModal" ><i class="fa fa-envelope-square"></i></button>
                                        <button type="button" class="btn btn-default" style="margin: 10%" data-dismiss="modal">Fechar</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                </form>
                 <script>
                function rastrearPedido'.$i.'() {
                $rastrear = $("#rastrear'.$i.'").val();
                $nota = $("#nota'.$i.'").val();
                $cliente = $("#cliente'.$i.'").val();
                window.open("rastreio.php?rastrear=" + $rastrear + "&nota=" + $nota + "&cliente=" + $cliente);
            }
                    $("#add'.$i.'").on("click" , function() {
                        $(".modal").modal("hide");
                      });
                      $("#add'.$x.'").on("click" , function() {
                        $(".modal").modal("hide");
                      });
            </script>
                </td></tr>';
            $consultar2 = $pdo->prepare("SELECT PEDIDOS_ITEM_PRE.GRADE, PRODUTOS.DESCRICAO,PEDIDOS_ITEM_PRE.QTD "
                    . "FROM PEDIDOS_ITEM_PRE "
                    . "INNER JOIN PRODUTOS ON (PEDIDOS_ITEM_PRE.COD_PRODUTO = PRODUTOS.CODIGO) "
                    . "WHERE PRODUTOS.NF_REMESSA='N' AND PEDIDO =".$resultado["PEDIDO"]);
            $consultar2->execute();
            echo "<tr class=\"linhaItens\">
                                        <td colspan=\"5\">
                                            <table class=\"tbItens\" rules=\"rows\">
                                                <thead>
                                                    <tr>
                                                        <th><p style=\"color: #808080; height: 10\">Produtos<p></th>
                                                        <th><p style=\"color: #808080; height: 10\">Grade<p></th>
                                                        <th><p style=\"color: #808080; height: 10\">Quantidade<p></th>
                                                    </tr>
                                                </thead>
                                                <tbody>";
            while($resultado2 = $consultar2->fetch(PDO::FETCH_ASSOC)){
                echo "<tr><td>".$resultado2['DESCRICAO']."</td>";
                echo "<td>".$resultado2['GRADE']."</td>";
                echo "<td>".$resultado2['QTD']."</td></tr>";
            }
            echo "</tbody></table></td></tr>";
        }
         echo "</tbody></table></div>";
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function historicoRastreio($idCustomer){
    $pdo = conecta();
    try {
        $consultar = $pdo->prepare("SELECT PEDIDOS_PRE.PEDIDO, PEDIDOS_PRE.EMISSAO, NOTAS.NRO_NOTA "
                . "FROM PEDIDOS_PRE "
                . "INNER JOIN PEDIDOS ON (PEDIDOS_PRE.PEDIDO = PEDIDOS.NRO_INTERNO) "
                . "INNER JOIN NOTAS ON (NOTAS.PEDIDO = PEDIDOS.PEDIDO) "
                . "WHERE PEDIDOS_PRE.COD_CLIENTE=:idCustomer AND PEDIDOS_PRE.PED_REQ = 'P' "
                . "order by PEDIDOS_PRE.PEDIDO DESC ");
        $consultar->bindValue(':idCustomer', $idCustomer, PDO::PARAM_INT);
        $consultar->execute();
        echo "<div id=\"teste\" style=\"overflow: scroll;border:1px\"><table id=\"tbVendas\" rules=\"rows\">
                                <thead>
                                    <tr>
                                        <th cellspacing=\"2\"></th>
                                        <th><p style=\"color: #808080; height: 10\">Nota<p></th>
                                        <th><p style=\"color: #808080; height: 10\">Data<p></th>
                                        <th><p style=\"color: #808080; height: 10\">Rastrear<p></th>
                                    </tr>
                                </thead>
                                <tbody>";
        $i = 0;
        $x = 0;
        while ($resultado = $consultar->fetch(PDO::FETCH_ASSOC)){
            $i++;
            $x++;
             echo "<tr class=\"linhaVenda\"><td></td>";
             echo "<td>".$resultado['NRO_NOTA']."</td>";             
             echo "<td>".date_format(new DateTime($resultado['EMISSAO']), "d/m/Y")."</td>";
             echo '<td>'
             . '<form method="post" id="form_rastreio'.$i.'">
                 <br/>
                    <input type="hidden" name="rastrear" id="rastrear'.$i.'" value="rastrear"/>
                    <input type="hidden" name="nota" id="nota'.$i.'" value="'.$resultado['NRO_NOTA'].'"/>
                    <button type="button" name="submit" id="submit" onclick="rastrearPedido'.$i.'()" class="btn btn-primar" ><i class="fa fa-truck"></i></button>
                    <input type="hidden" name="cliente" id="cliente'.$i.'" value="'.$idCustomer.'"/>
                </form>
                <form ajax="true">
                    <div class="modal fade" id="Modal_qtd'.$i.'"  role="dialog">
                                            <div class="modal-dialog">
                                              <div class="modal-content">
                                                <div class="modal-header" style="text-align: center;">
                                                  <button type="button" class="close" data-dismiss="modal" style="margin:3%">&times;</button>
                                                  <h4 class="modal-title"><label style="margin:3%">Enviar nota por email</label></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="email" name="email" value="" placeholder="Inserir E-mail"/>       
                                                    <input type="hidden" name="nota" id="nota" value="'.$resultado['NRO_NOTA'].'" placeholder="."/>
                                                        <br/><br/>
                                                    <input type="radio" name="acao" id="pdf" value="pdf" checked> <label for="pdf">PDF</label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input type="radio" name="acao" id="xml" value="xml"> <label for="xml">XML</label>
                                                    <br/>
                                               </div>
                                               <div class="modal-footer">
                                        <button type="submit" id="add'.$i.'" style="width:30%" class="btn btn-primary" data-toggle="modal" data-target="#myModal" ><i class="fa fa-envelope-square"></i></button>
                                        <button type="button" class="btn btn-default" style="margin: 10%" data-dismiss="modal">Fechar</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                    </form>
                 <script>
                function rastrearPedido'.$i.'() {
                $rastrear = $("#rastrear'.$i.'").val();
                $nota = $("#nota'.$i.'").val();
                $cliente = $("#cliente'.$i.'").val();
                window.open("rastreio.php?rastrear=" + $rastrear + "&nota=" + $nota + "&cliente=" + $cliente);
            }
                    $("#add'.$i.'").on("click" , function() {
                        $(".modal").modal("hide");
                      });
                      $("#add'.$x.'").on("click" , function() {
                        $(".modal").modal("hide");
                      });
            </script>
                </td></tr>';
            $consultar2 = $pdo->prepare("SELECT PEDIDOS_ITEM_PRE.GRADE, PRODUTOS.DESCRICAO,PEDIDOS_ITEM_PRE.QTD "
                    . "FROM PEDIDOS_ITEM_PRE "
                    . "INNER JOIN PRODUTOS ON (PEDIDOS_ITEM_PRE.COD_PRODUTO = PRODUTOS.CODIGO) "
                    . "WHERE PRODUTOS.NF_REMESSA='N' AND PEDIDO =".$resultado["PEDIDO"]);
            $consultar2->execute();
            echo "<tr class=\"linhaItens\">
                                        <td colspan=\"5\">
                                            <table class=\"tbItens\" rules=\"rows\">
                                                <thead>
                                                    <tr>
                                                        <th><p style=\"color: #808080; height: 10\">Produtos<p></th>
                                                        <th><p style=\"color: #808080; height: 10\">Grade<p></th>
                                                        <th><p style=\"color: #808080; height: 10\">Quantidade<p></th>
                                                    </tr>
                                                </thead>
                                                <tbody>";
            while($resultado2 = $consultar2->fetch(PDO::FETCH_ASSOC)){
                echo "<tr><td>".$resultado2['DESCRICAO']."</td>";
                echo "<td>".$resultado2['GRADE']."</td>";
                echo "<td>".$resultado2['QTD']."</td></tr>";
            }
            echo "</tbody></table></td></tr>";
        }
         echo "</tbody></table></div>";
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}


?>
