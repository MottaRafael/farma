<?php
session_start();
$_SESSION['modal'] = true;
require 'crud.php';

$pdo = conecta();

if (isset($_GET['email'])) {
    $teste2 = $_GET['email'];
    $tes = $pdo->prepare("select CODIGO, NRO_LOJAS from CLIENTES where EMAIL_APP='" . $teste2 . "'");
    $tes->execute();
    $testando = $tes->fetch(PDO::FETCH_ASSOC);
    $idCustomer = $testando['CODIGO'];
} elseif (isset($_GET['codigo'])) {
    $idCustomer = $_GET['codigo'];
}
?>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0,  maximum-scale=1.0, minimum-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <style>
            #tbVendas{
                width:90%;
                border:solid 0.5px;
                background:#f2f2f2;
                margin-right:auto; 
                margin-left:auto;
                border-color: #808080;
            }

            .tbItens{
                width:95%;
                border:solid 0.5px;
                background:#f2f2f2;
                margin-left: 2.5%;
                margin-bottom: 2.5%;
                margin-top: 2.5%;
                border-color: #808080;
            }

            #tbVendas thead th, .tbItens thead th{
                text-align:center;
                border-color: #808080;
            }

            #tbVendas td{
                height: 30px;
                border-color: #808080;
            }

            #tbVendas tr td, .tbItens tr td{
                border:solid 0.5px;
                border-right: none;
                text-align:center;
                border-color: #808080;
            }

            #tbVendas tr td:nth-child(1){
                background:#f2f2f2;
                border-color: #808080;
            }

            #tbVendas tbody tr{
                background:#ffffff;
                border-color: #808080;
            }

            .tbItens tr{
                background:#f2f2f2;
                height: 20px;
                border-color: #808080;
            }

            .tbItens td{
                background:#f2f2f2;
                border-color: #808080;
            }

            #tbVendas thead{
                background:#ffffff;
            }

            .linhaItens{
                display: none;
            }

            .tbItens{
                font-size:13px;
            }

            #div_total{
                background:#ffffff;
            }
            
            #principal {
                display: none;
            }
            
            .btn-primar {
            color: #fff;
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          
          .btn-primar:focus,
          .btn-primar.focus {
            color: #fff;
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          
          .btn-primar:hover {
            color: #fff;
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          
          .btn-primar:active,
          .btn-primar.active,
          .open .dropdown-toggle.btn-primar {
            color: #fff;
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          
          .btn-primar:active:hover,
          .btn-primar.active:hover,
          .open .dropdown-toggle.btn-primar:hover,
          .btn-primar:active:focus,
          .btn-primar.active:focus,
          .open .dropdown-toggle.btn-primar:focus,
          .btn-primar:active.focus,
          .btn-primar.active.focus,
          .open .dropdown-toggle.btn-primar.focus {
            color: #fff;
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          
          .btn-primar:active,
          .btn-primar.active,
          .open .dropdown-toggle.btn-primar {
            background-image: none;
          }
          
          .btn-primar.disabled:hover,
          .btn-primar[disabled]:hover,
          fieldset[disabled] .btn-primar:hover,
          .btn-primar.disabled:focus,
          .btn-primar[disabled]:focus,
          fieldset[disabled] .btn-primar:focus,
          .btn-primar.disabled.focus,
          .btn-primar[disabled].focus,
          fieldset[disabled] .btn-primar.focus {
            background-color: #00B4BD;
            border-color: #00B4BD;
          }
          
          .btn-primar .badge {
            color: #00B4BD;
            background-color: #fff;
          }
          
        </style>
    </head>
    <script>
        if (document.readyState) {
            document.onreadystatechange = checkstate;
        } else if (document.addEventListener) {
            document.addEventListener("DOMContentLoaded", saydone, false);
        }
        
        function checkstate() {
            if (document.readyState == "complete" || document.readyState == "complete") {
                document.getElementById("principal").style.display = "block";
            }
        }
        
        function saydone() {
            document.getElementById("principal").style.display = "block";
        }
        
        $(document).ready(function () {
            $(".linhaVenda td:nth-child(1)").html("<img src='icone/details_open.png' class='btnDetalhe'/>");
            $(".btnDetalhe").click(function () {
                var indice = $(this).parent().parent().index();
                var detalhe = $(this).parent().parent().next();
                var status = $(detalhe).css("display");
                if (status == "none")
                    $(detalhe).css("display", "table-row");
                else
                    $(detalhe).css("display", "none");
            });
            $('form[ajax=true]').submit(function (e) {
                e.preventDefault();
                var dados = $(this).serialize();
                $.ajax({
                    type: "POST",
                    url: "envia_nota.php",
                    data: dados,
                    success: function () {
                        $("#myModal").modal('show');
                        setTimeout(function () {
                            $('#myModal').modal('hide');}, 500);
                    }
                });
                return false;
            });
        });
        
    </script>
    
    <body style="background-color: #FFFFFF" id="principal">
        
        <br/>
        <?php historicoRastreio($idCustomer); ?>
        <br/>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <br/>
                        <div class="alert alert-success" role="alert">
                            <strong><p style="padding-left: 10"> Enviando...</p></strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <form method="GET" id="exemplo_sug" action="<?php $_SERVER['PHP_SELF']; ?>">
            <?php
            $sql_novo = $pdo->prepare("select OUTRAS_RAZOES.cod_razao, clientes.matriz_filial as MATRIZ from clientes "
                    . "inner join outras_razoes on (clientes.codigo = outras_razoes.cod_razao) "
                    . "where OUTRAS_RAZOES.cod_cliente=" . $idCustomer);
            $sql_novo->execute();
            $testando2 = $sql_novo->fetch(PDO::FETCH_ASSOC);
            $matriz_filial = $testando2['MATRIZ'];
            $matriz = trim($matriz_filial);
            $et[] = $testando2['COD_RAZAO'];
            $et2 = count($et);
            if (($et2 >= 1) && (empty($_SESSION['razao'])) && (($matriz == "M") || ($matriz == "F"))) {
                ?>
            
                <script>
                    $("#botao").click(function () {
                        $('#exemplomodal_sug').modal('show');
                    });
                </script>
                
                <?php
                $novo = $pdo->prepare("select OUTRAS_RAZOES.cod_razao,CLIENTES.cgc,CLIENTES.ENDERECOFATURAMENTO, CLIENTES.NOME "
                        . "from clientes "
                        . "inner join outras_razoes on (clientes.codigo = outras_razoes.cod_razao) "
                        . "where OUTRAS_RAZOES.cod_cliente=" . $idCustomer);
                $novo->execute();
                $novo2 = $pdo->prepare("select NOME, CODIGO, CGC, ENDERECOFATURAMENTO FROM CLIENTES WHERE CODIGO=" . $idCustomer);
                $novo2->execute();
                $novo_sql2 = $novo2->fetch(PDO::FETCH_ASSOC);
                echo '<div class="modal fade" id="exemplomodal_sug"  role="dialog">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header" style="text-align: center;">
                                  <h4 class="modal-title"><label style="margin:3%">Escolha a loja para visualizar o status</label></h4>
                                </div>
                                <div class="modal-body"><br/>
                                <label for="nome"><font size="1">RAZÃO: ' . $novo_sql2['NOME'] . '</font></label><br/>'
                . '<input type="radio" style="float: right;" value="' . $idCustomer . '" id="nome" name="codigo" checked/>'
                . '<label for="nome"><font size="1"> CNPJ: ' . $novo_sql2['CGC'] . '</font></label><br/>'
                . '<label for="nome"><font size="1">' . $novo_sql2['ENDERECOFATURAMENTO'] . '</font></label><br/>'
                . '<hr/>';
                $y = 0;
                while ($novo_sql = $novo->fetch(PDO::FETCH_ASSOC)) {
                    $y++;
                    echo
                    '<label for="nome' . $y . '"><font size="1">RAZÃO: ' . $novo_sql['NOME'] . '</font></label><br/>'
                    . '<input type="radio"  style="float: right;" value="' . $novo_sql['COD_RAZAO'] . '" id="nome' . $y . '" name="codigo"/>'
                    . '<label for="nome' . $y . '"><font size="1"> CNPJ: ' . $novo_sql['CGC'] . '</font></label><br/>'
                    . '<label for="nome' . $y . '"><font size="1">' . $novo_sql['ENDERECOFATURAMENTO'] . '</font></label>'
                    . '<hr/>';
                }
                echo '<br/>
                        <div class="modal-footer">
                        <input type="submit" id="addexemplo" style="width:30%" class="btn btn-primary" value="OK">
                    </div>
                  </div>
                </div>
              </div>';
            }
            ?>
        </form>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>