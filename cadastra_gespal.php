<?php 
session_start();
require_once ('classes/conexao.php');
date_default_timezone_set('America/Sao_Paulo');
$datahora = date('d.m.Y H:i');
$data = date('d.m.Y');

$pdo = conecta();

$cliente = $_POST['cliente'];
$frete = $_POST['frete'];

$codigo = $_POST['codigo'];
$array = explode(",", $codigo);

$grade = $_POST['grade'];
$array2 = explode(",", $grade);

$ipi = $_POST['ipi'];
$array3 = explode(",", $ipi);

$preco = $_POST['preco'];
$array4 = explode(",", $preco);

$qtd = $_POST['qtd'];
$array5 = explode(",", $qtd);

$desconto = $_POST['desconto'];
$array6 = explode(",", $desconto);

$st = $_POST['st'];
$array7 = explode(",", $st);

$zero = 0;
$ponto = '.';
$n = 'N';
$p = 'P';
$s = 'S';

if(empty($_POST['ckb'])){
//    $ckb = '.';
      $ckb = 'Este pedido é um teste. Não liberar para faturamento';
}else{
//    $ckb = $_POST['ckb'];
    $ckb = 'Este pedido é um teste. Não liberar para faturamento';
}
$radio = $_POST['radio'];

if($radio == '30-60-90'){
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "90";
}elseif($radio == '30-60'){
    $cond1 = "30";
    $cond2 = "60";
    $cond3 = "0";
}elseif($radio == '45-75-105'){
    $cond1 = "45";
    $cond2 = "75";
    $cond3 = "105";
}elseif($radio == '45-75'){
    $cond1 = "45";
    $cond2 = "75";
    $cond3 = "0";
}elseif($radio == '15'){
    $cond1 = "15";
    $cond2 = "0";
    $cond3 = "0";
}elseif($radio == '20'){
    $cond1 = "20";
    $cond2 = "0";
    $cond3 = "0";
}elseif($radio == '30'){
    $cond1 = "30";
    $cond2 = "0";
    $cond3 = "0";
}elseif($radio == '45'){
    $cond1 = "45";
    $cond2 = "0";
    $cond3 = "0";
}

$sql = $pdo->prepare('SELECT GEN_ID(gen_pedidos_pre_id, 1) as PED FROM RDB$DATABASE');
$sql->execute();
$pedido = $sql->fetch(PDO::FETCH_ASSOC);

$select = $pdo->prepare("SELECT REPRESENTANTE, COD_VENDEDOR, COD_CARTEIRA FROM CLIENTES WHERE CODIGO=". $cliente);
$select ->execute();
$dados = $select->fetch(PDO::FETCH_ASSOC);

$select2 = $pdo->prepare("SELECT COD_USUARIO, NOME FROM USUARIOS WHERE COD_CARTEIRA=".$dados['COD_CARTEIRA']);
$select2 ->execute();
$dados2 = $select2->fetch(PDO::FETCH_ASSOC);

$select3 = $pdo->prepare("SELECT COD_USUARIO, NOME FROM USUARIOS WHERE COD_USUARIO=956");
$select3 ->execute();
$dados3 = $select3->fetch(PDO::FETCH_ASSOC);

$operador = $dados3['NOME'];

 try {
 $sql2 = ("insert into pedidos_pre (PEDIDO, COD_CLIENTE, COD_REPRES, COD_VEND, COD_SUPERVISOR, EMISSAO, ENTREGA, "
 . "OC, OPERADOR, PED_REPRES, PED_REQ, COND1, COND2, COND3, COND4, COND5, COND6, IMPRESSO, LIBERADO_IMPRESSAO, CANCELADO, GRAVADO_NAPHTA, "
 . "LIBERADO_CADASTRO, STATUS, VISTO, COD_REQUISICAO, COD_AMOSTRA, CARTEIRA, OBS, OBS_FINANCEIRO, OBS_COMERCIAL, EMPRESA, DE_ONDE, PERSONALIZADO, VALOR_FRETE) "
 . "values (:PEDIDO, :COD_CLIENTE, :COD_REPRES, :COD_VEND, :COD_SUPERVISOR, :EMISSAO, :ENTREGA, :OC, :OPERADOR, :PED_REPRES, :PED_REQ,"
         . " :COND1, :COND2, :COND3, :COND4, :COND5, :COND6, :IMPRESSO, :LIBERADO_IMPRESSAO, :CANCELADO, :GRAVADO_NAPHTA, :LIBERADO_CADASTRO,"
         . " :STATUS, :VISTO, :COD_REQUISICAO, :COD_AMOSTRA, :CARTEIRA, :OBS, :OBS_FINANCEIRO, :OBS_COMERCIAL, :EMPRESA, :DE_ONDE, :PERSONALIZADO, :VALOR_FRETE)");
   $stmt = $pdo->prepare($sql2);
        $stmt->bindParam(':PEDIDO', $pedido['PED'], PDO::PARAM_INT);
        $stmt->bindParam(':COD_CLIENTE', $cliente, PDO::PARAM_INT);
        $stmt->bindParam(':COD_REPRES', $dados['REPRESENTANTE'], PDO::PARAM_STR);
        $stmt->bindParam(':COD_VEND', $zero, PDO::PARAM_STR);
        $stmt->bindParam(':COD_SUPERVISOR', $zero, PDO::PARAM_STR);
        $stmt->bindParam(':EMISSAO', $datahora, PDO::PARAM_STR);
        $stmt->bindParam(':ENTREGA', $data, PDO::PARAM_STR);
        $stmt->bindParam(':OC', $ponto, PDO::PARAM_STR);
        $stmt->bindParam(':OPERADOR', $operador, PDO::PARAM_STR);
        $stmt->bindParam(':PED_REPRES', $n, PDO::PARAM_STR);
        $stmt->bindParam(':PED_REQ', $p, PDO::PARAM_STR);
        $stmt->bindParam(':COND1', $cond1, PDO::PARAM_STR);
        $stmt->bindParam(':COND2', $cond2, PDO::PARAM_STR);
        $stmt->bindParam(':COND3', $cond3, PDO::PARAM_STR);
        $stmt->bindParam(':COND4', $zero, PDO::PARAM_STR);
        $stmt->bindParam(':COND5', $zero, PDO::PARAM_STR);
        $stmt->bindParam(':COND6', $zero, PDO::PARAM_STR);
        $stmt->bindParam(':IMPRESSO', $n, PDO::PARAM_STR);
        $stmt->bindParam(':LIBERADO_IMPRESSAO', $n, PDO::PARAM_STR);
        $stmt->bindParam(':CANCELADO', $n, PDO::PARAM_STR);
        $stmt->bindParam(':GRAVADO_NAPHTA', $n, PDO::PARAM_STR);
        $stmt->bindParam(':LIBERADO_CADASTRO', $n, PDO::PARAM_STR);
        $stmt->bindParam(':STATUS', $zero, PDO::PARAM_STR);
        $stmt->bindParam(':VISTO', $s, PDO::PARAM_STR);
        $stmt->bindParam(':COD_REQUISICAO', $zero, PDO::PARAM_STR);
        $stmt->bindParam(':COD_AMOSTRA', $zero, PDO::PARAM_STR);
        $stmt->bindParam(':CARTEIRA', $dados['COD_CARTEIRA'], PDO::PARAM_STR);
        $stmt->bindParam(':OBS', $ckb, PDO::PARAM_STR);
        $stmt->bindParam(':OBS_FINANCEIRO', $ponto, PDO::PARAM_STR);
        $stmt->bindParam(':OBS_COMERCIAL', $ponto, PDO::PARAM_STR);
        $stmt->bindParam(':EMPRESA', $zero, PDO::PARAM_STR);
        $stmt->bindParam(':DE_ONDE', $p, PDO::PARAM_STR);
        $stmt->bindParam(':PERSONALIZADO', $n, PDO::PARAM_STR);
        $stmt->bindParam(':VALOR_FRETE', $frete, PDO::PARAM_STR);
        $stmt->execute();
        
        for($i = 0; $i < count($array);$i ++){

 $sql3 = ("insert into pedidos_item_pre (PEDIDO,COD_PRODUTO, GRADE, QTD, UNITARIO,IPI, DESCONTO, PERSONALIZADO, APLICOU_ACRESCIMO,VALOR_ICMS_SUBST_TRIB) "
 . "values (:PEDIDO, :COD_PRODUTO, :GRADE, :QTD, :UNITARIO, :IPI, :DESCONTO, :PERSONALIZADO, :APLICOU_ACRESCIMO, :VALOR_ICMS_SUBST_TRIB)");
  $stmt2 = $pdo->prepare($sql3);
       $stmt2->bindParam(':PEDIDO', $pedido['PED'], PDO::PARAM_INT);
       $stmt2->bindParam(':COD_PRODUTO', $array[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':GRADE',  $array2[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':QTD',  $array5[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':UNITARIO', $array4[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':IPI', $array3[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':DESCONTO', $array6[$i], PDO::PARAM_STR);
       $stmt2->bindParam(':PERSONALIZADO', $n, PDO::PARAM_STR);
       $stmt2->bindParam(':APLICOU_ACRESCIMO', $n, PDO::PARAM_STR);
       $stmt2->bindParam(':VALOR_ICMS_SUBST_TRIB', $array7[$i], PDO::PARAM_STR);
       $stmt2->execute();
        
}

    session_destroy();

    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    

